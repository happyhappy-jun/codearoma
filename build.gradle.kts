// Copyright 2000-2023 JetBrains s.r.o. and contributors. Use of this source code is governed by the Apache 2.0 license.

plugins {
    idea
    java
    jacoco
    id("org.jetbrains.intellij") version "1.16.0"
}

group = "org.intellij.sdk"
version = "0.0.1"
java.sourceCompatibility = JavaVersion.VERSION_17

repositories {
    mavenCentral()
}

dependencies {
    testImplementation("org.junit.jupiter:junit-jupiter:5.8.1")
    testImplementation("org.mockito:mockito-core:5.2.0")
    implementation("org.commonmark:commonmark:0.20.0")
    implementation("org.json:json:20230227")
    implementation("com.knuddels:jtokkit:0.6.1")

}

jacoco {
    toolVersion = "0.8.9"
}

// See https://plugins.jetbrains.com/docs/intellij/tools-gradle-intellij-plugin.html
intellij {
    version.set("2022.3.3")
    plugins.set(listOf("com.intellij.java"))
}

tasks {
    test {
        finalizedBy(jacocoTestReport)
    }

    withType<Test> {
        configure<JacocoTaskExtension> {
            isIncludeNoLocationClasses = true
            excludes = listOf("jdk.internal.*")
        }
    }

    jacocoTestReport {
        reports {
            html.required.set(true)
            xml.required.set(true)
        }
        classDirectories.setFrom(instrumentCode)
    }

    jacocoTestCoverageVerification {
        classDirectories.setFrom(instrumentCode)
    }

    buildSearchableOptions {
        enabled = false
    }

    patchPluginXml {
        version.set("${project.version}")
        sinceBuild.set("223")
        untilBuild.set("232.*")
    }
}
