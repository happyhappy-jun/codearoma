package com.gitlab.team21.codearoma.chat;

import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.List;

/**
 * A list of listeners that will be notified when a chat event occurs.
 */
public class ListenerList {

    private final List<ChatListener> listeners = new ArrayList<>();
    private ChatListener proxy;

    /**
     * Add a listener that will be notified when a chat event occurs.
     *
     * @param listener The listener to be added
     */
    public void addListener(ChatListener listener) {
        synchronized (listeners) {
            listeners.add(listener);
            proxy = null;
        }
    }

    /**
     * Remove a listener that will be notified when a chat event occurs.
     *
     * @param listener The listener to be removed
     */
    public void removeListener(ChatListener listener) {
        synchronized (listeners) {
            listeners.remove(listener);
            proxy = null;
        }
    }

    /**
     * @return A proxy that will notify all listeners when a chat event occurs
     */
    public ChatListener fire() {
        synchronized (listeners) {
            if (proxy == null) {
                proxy = (ChatListener) Proxy.newProxyInstance(
                        ChatListener.class.getClassLoader(),
                        new Class<?>[]{ChatListener.class},
                        (proxy, method, args) -> {
                            for (ChatListener listener : listeners) {
                                method.invoke(listener, args);
                            }
                            return null; // void return type for listener methods
                        });
            }
            return proxy;
        }
    }
}
