package com.gitlab.team21.codearoma.chat;

public interface ChatListener {
    void sending(ChatEvent.Sending event);

    void failed(ChatEvent.Failed event);

    void responseArrived(ChatEvent.ResponseArrived event);
}
