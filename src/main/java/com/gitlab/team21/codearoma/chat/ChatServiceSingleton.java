package com.gitlab.team21.codearoma.chat;

/**
 * Singleton class for ChatService.
 * It is used to create only one instance of ChatService.
 * The class is initialized from @see plugin.xml
 */
public class ChatServiceSingleton {
    private ChatService chatService;

    /**
     * @return The instance of ChatService
     */
    public ChatService getChatServiceInstance() {
        if (chatService == null) {
            chatService = new ChatService();
        }
        return chatService;
    }
}
