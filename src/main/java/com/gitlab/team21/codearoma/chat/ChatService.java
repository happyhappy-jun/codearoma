package com.gitlab.team21.codearoma.chat;

import com.gitlab.team21.codearoma.llm.CallLLMApi;
import com.intellij.openapi.application.ApplicationManager;
import org.apache.http.impl.client.HttpClients;

import java.util.concurrent.Future;

/**
 * Service for sending messages to LLM.
 * The concrete class for subject of observer pattern.
 */
public class ChatService extends ChatSubject {
    CallLLMApi callLLMApi;

    public ChatService() {
        this.callLLMApi = new CallLLMApi(HttpClients.createDefault());
    }

    public ChatService(CallLLMApi callLLMApi) {
        this.callLLMApi = callLLMApi;
    }

    /**
     * @param conversationContext The context of the conversation
     * @param message             The message to be sent
     * @return A future object of the message sending
     */
    public Future sendMessage(String conversationContext, String message) {
        ChatListener listener = this.listeners.fire();
        ChatEvent.Sending event = ChatEvent.sending(this, message);
        listener.sending(event);
        // sending message to LLM
        return ApplicationManager.getApplication().executeOnPooledThread(() -> {
            try {
                String response = this.callLLMApi.callApi(conversationContext);
                callLLMApi.setHttpClient(HttpClients.createDefault());
                listener.responseArrived(event.responseArrived(response));
            } catch (Exception e) {
                callLLMApi.setHttpClient(HttpClients.createDefault());
                listener.failed(event.failed(e));
            }
        });
    }
}
