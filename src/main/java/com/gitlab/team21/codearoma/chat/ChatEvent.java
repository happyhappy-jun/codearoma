package com.gitlab.team21.codearoma.chat;

import java.util.EventObject;

/**
 * Event definition for chat events.
 */
public abstract class ChatEvent extends EventObject {
    private final String message;

    protected ChatEvent(ChatService eventSource, String message) {
        super(eventSource);
        this.message = message;
    }

    /**
     * @param source  The source of the event
     * @param message The message to be sent
     * @return A new Sending event
     */
    public static Sending sending(ChatService source, String message) {
        return new Sending(source, message);
    }

    /**
     * @return The name of the method that triggered the event
     */
    public abstract String getMethodName();

    /**
     * @return The message to be sent
     */
    public String getMessage() {
        return message;
    }

    /**
     * @return The source of the event
     */
    public final ChatService getEventSource() {
        return (ChatService) super.getSource();
    }

    /**
     * Event for sending a message.
     */
    public static class Sending extends ChatEvent {
        /**
         * @param source  The source of the event
         * @param message The message to be sent
         */
        public Sending(ChatService source, String message) {
            super(source, message);
        }

        protected Sending(Sending sourceEvent) {
            super(sourceEvent.getEventSource(), sourceEvent.getMessage());
        }

        /**
         * @param message The response message
         * @return A new ResponseArrived event
         */
        public ResponseArrived responseArrived(String message) {
            return new ResponseArrived(this, message);
        }

        /**
         * @param cause The cause of the failure
         * @return A new Failed event
         */
        public Failed failed(Throwable cause) {
            return new Failed(this, cause);
        }

        /**
         * @return The name of the method that triggered the event
         */
        @Override
        public String getMethodName() {
            return "sending";
        }
    }

    /**
     * Event for a failed message.
     */
    public static class Failed extends Sending {
        private final Throwable cause;

        protected Failed(Sending source, Throwable cause) {
            super(source);
            this.cause = cause;
        }

        /**
         * @return The cause of the failure
         */
        public Throwable getCause() {
            return cause;
        }

        /**
         * @return The name of the method that triggered the event
         */
        @Override
        public String getMethodName() {
            return "failed";
        }
    }

    /**
     * Response Arrived event.
     */
    public static class ResponseArrived extends Sending {
        private final String response;

        protected ResponseArrived(Sending source, String response) {
            super(source);
            this.response = response;
        }

        /**
         * @return The response message
         */
        @Override
        public String getMessage() {
            return response;
        }

        /**
         * @return The name of the method that triggered the event
         */
        @Override
        public String getMethodName() {
            return "responseArrived";
        }
    }
}
