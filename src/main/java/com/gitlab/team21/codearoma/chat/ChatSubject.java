package com.gitlab.team21.codearoma.chat;

import org.jetbrains.annotations.NotNull;

/**
 * A subject that can be observed by a {@link ChatListener}.
 */
public class ChatSubject {

    protected ListenerList listeners = new ListenerList();

    /**
     * Adds an observer to the set of observers, provided that it is not already added.
     *
     * @param o an observer
     */
    public void addChatListener(@NotNull ChatListener o) {
        listeners.addListener(o);
    }
}

