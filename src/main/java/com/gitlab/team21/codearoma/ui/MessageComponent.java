package com.gitlab.team21.codearoma.ui;

import com.intellij.ui.JBColor;
import com.intellij.ui.components.JBPanel;
import com.intellij.ui.components.panels.VerticalLayout;
import com.intellij.util.ui.JBUI;

import javax.swing.*;
import java.awt.*;

/**
 * Message Component Class
 * <p>
 * This class is for highest level of ui for user and llm message component.
 * It contains the message panel and the message content.
 * </p>
 *
 * @author Byungjun Yoon
 */
public class MessageComponent extends JBPanel<MessageComponent> {
    private final MessagePanel component = new MessagePanel();

    public MessageComponent(String text, boolean isUser) {
        setDoubleBuffered(true);
        setOpaque(true);
        setBackground(isUser ? new JBColor(0xF7F7F7, 0x3C3F41) : new JBColor(0xEBEBEB, 0x2d2f30));
        setBorder(JBUI.Borders.empty(JBUI.scale(4), JBUI.scale(1)));
        setLayout(new BorderLayout(JBUI.scale(2), 0));

        JPanel centerPanel = new JPanel(new VerticalLayout(JBUI.scale(0)));
        centerPanel.setOpaque(false);
        centerPanel.setBorder(JBUI.Borders.emptyLeft(JBUI.scale(5)));
        centerPanel.add(createContentComponent(text));
        add(centerPanel, BorderLayout.CENTER);
    }

    /**
     * @param content message content
     * @return message content component
     */
    public Component createContentComponent(String content) {
        component.setText(content);
        component.setOpaque(false);
        component.setBorder(null);
        component.revalidate();
        component.repaint();

        return component;
    }

    /**
     * Update message content
     *
     * @param text message content
     */
    public void setText(String text) {
        component.setText(text);
    }
}
