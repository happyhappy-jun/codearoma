package com.gitlab.team21.codearoma.ui;

import com.intellij.ui.Gray;
import com.intellij.ui.JBColor;
import com.intellij.ui.components.JBLabel;
import com.intellij.ui.components.JBPanel;
import com.intellij.ui.components.JBScrollPane;
import com.intellij.ui.components.panels.VerticalLayout;
import com.intellij.util.ui.JBFont;
import com.intellij.util.ui.JBUI;
import com.intellij.util.ui.UIUtil;

import javax.swing.*;
import java.awt.*;


/**
 * Message Group Component Class
 * <p>
 * This class collect multiple MessageComponent and display in scrollable panel.
 * It contains the message list and the scroll pane.
 * </p>
 *
 * @author Byungjun Yoon
 * @see MessageComponent
 */
public class MessageGroupComponent extends JBPanel<MessageGroupComponent> {
    private final JPanel myList = new JPanel(new VerticalLayout(0));
    private final JBScrollPane myScrollPane = new JBScrollPane(myList, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
            ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

    public MessageGroupComponent() {
        setBorder(JBUI.Borders.empty());
        setLayout(new BorderLayout());
        setBackground(UIUtil.getListBackground());

        myScrollPane.getVerticalScrollBar().putClientProperty(JBScrollPane.IGNORE_SCROLLBAR_IN_INSETS, Boolean.TRUE);
        JPanel mainPanel = new JPanel(new BorderLayout(0, 0));
        mainPanel.setOpaque(false);
        mainPanel.setBorder(JBUI.Borders.emptyLeft(0));

        add(mainPanel, BorderLayout.CENTER);
        JBLabel myTitle = new JBLabel("Conversation");
        myTitle.setForeground(JBColor.namedColor("Label.infoForeground", new JBColor(Gray.x80, Gray.x8C)));
        myTitle.setFont(JBFont.label());

        JPanel panel = new JPanel(new BorderLayout());
        panel.setBorder(JBUI.Borders.empty(0, 10, 10, 0));

        panel.add(myTitle, BorderLayout.WEST);
        mainPanel.add(panel, BorderLayout.NORTH);

        myList.setOpaque(true);
        myList.setBackground(UIUtil.getListBackground());
        myList.setBorder(JBUI.Borders.emptyRight(0));

        myScrollPane.setBorder(JBUI.Borders.empty());
        mainPanel.add(myScrollPane);
        myScrollPane.getVerticalScrollBar().setAutoscrolls(true);

        myScrollPane.setBorder(JBUI.Borders.empty());
        mainPanel.add(myScrollPane);
        myScrollPane.getVerticalScrollBar().setAutoscrolls(true);
    }

    /**
     * Add the message component to the message group component
     * <p>
     * This method is for add the message component to the message group component.
     * This method is called when the user sends a message or LLM give response to user.
     * </p>
     *
     * @param messageComponent the message component
     */
    public void add(MessageComponent messageComponent) {
        Rectangle rect = messageComponent.getBounds();
        rect.y = myList.getHeight() - rect.height; // Scroll to the bottom

        SwingUtilities.invokeLater(() -> {
            myList.add(messageComponent);
            myScrollPane.getViewport().scrollRectToVisible(rect);
            refresh();
        });
    }

    /**
     * Refresh the message group component
     * <p>
     * This method is for refresh the message group component.
     * There is a issue of painting the message group component on plugin initialization.
     * So, this method is for refresh the message group component.
     * This method is called when the plugin is initialized.
     * </p>
     */
    public void refresh() {
        SwingUtilities.invokeLater(() -> {
            updateLayout();
            invalidate();
            validate();
            repaint();
        });
    }

    /**
     * Update the layout of the message group component
     * <p>
     * This method is for update the layout of the message group component.
     * This method is called when the user sends a message or LLM give response to user.
     * </p>
     */
    public void updateLayout() {
        LayoutManager layout = myList.getLayout();
        int componentCount = myList.getComponentCount();
        for (int i = 0; i < componentCount; i++) {
            layout.removeLayoutComponent(myList.getComponent(i));
            layout.addLayoutComponent(null, myList.getComponent(i));
        }
    }
}
