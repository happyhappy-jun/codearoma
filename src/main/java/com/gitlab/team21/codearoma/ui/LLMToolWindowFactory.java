package com.gitlab.team21.codearoma.ui;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowFactory;
import com.intellij.ui.content.Content;
import com.intellij.ui.content.ContentFactory;
import org.jetbrains.annotations.NotNull;

/**
 * Factory class for creating the tool window
 * This class is initialized when the plugin is loaded
 */
public class LLMToolWindowFactory implements ToolWindowFactory {

    /**
     * @param project    current project
     * @param toolWindow tool window
     */
    @Override
    public void createToolWindowContent(@NotNull Project project, @NotNull ToolWindow toolWindow) {
        LLMToolWindowContent toolWindowContent = new LLMToolWindowContent();
        Content content = ContentFactory.getInstance()
                .createContent(toolWindowContent.getContentPanel(), "CodeAroma", false);
        toolWindow.getContentManager().addContent(content);
    }
}
