package com.gitlab.team21.codearoma.ui;


import org.commonmark.node.Node;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;

import javax.swing.*;
import javax.swing.text.html.HTMLDocument;
import java.awt.*;


/**
 * Message Panel Class
 * <p>
 * The primitive class for message panel.
 * It contains raw string message of user
 * </p>
 *
 * @author Byungjun Yoon
 */
public class MessagePanel extends JEditorPane {
    public MessagePanel() {
        setEditable(false);
        setFocusable(false);
        setOpaque(false);
        setContentType("text/html");
        setEditable(false);
        setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

        Font font = UIManager.getFont("Label.font").deriveFont(14f);
        String bodyRule = "body { font-family: " + font.getFamily() + "; " +
                "font-size: " + font.getSize() + "pt; }";
        ((HTMLDocument) getDocument()).getStyleSheet().addRule(bodyRule);
    }

    /**
     * Update message content
     *
     * @param t message content
     */
    @Override
    public void setText(String t) {
        Parser parser = Parser.builder().build();
        Node document = parser.parse(t);
        HtmlRenderer renderer = HtmlRenderer.builder().build();


        String html = renderer.render(document);
        super.setText(html);
    }
}
