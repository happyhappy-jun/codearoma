package com.gitlab.team21.codearoma.ui;

import com.gitlab.team21.codearoma.chat.ChatEvent;
import com.gitlab.team21.codearoma.chat.ChatListener;
import com.gitlab.team21.codearoma.chat.ChatService;
import com.gitlab.team21.codearoma.chat.ChatServiceSingleton;
import com.gitlab.team21.codearoma.llm.model.ConversationManager;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.ui.OnePixelSplitter;
import com.intellij.ui.components.fields.ExpandableTextField;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import static java.awt.event.InputEvent.CTRL_DOWN_MASK;

/**
 * LLM Tool Window Content Class
 * <p>
 * This class is responsible for the content of the tool window.
 * It contains the message group component, the search text field,
 * the send button, the action panel, and the progress bar.
 * </p>
 *
 * @author Jad El Idrissi Dafali
 * @author Byungjun Yoon
 */
public class LLMToolWindowContent implements ChatListener {

    private final MessageGroupComponent contentPanel;
    private final OnePixelSplitter splitter;
    private final JPanel actionPanel;
    private final JProgressBar progressBar;
    private final ExpandableTextField textField;
    private final ConversationManager conversationManager;
    private volatile MessageComponent answer;

    public LLMToolWindowContent() {
        ChatService chatService = ApplicationManager.getApplication().getService(ChatServiceSingleton.class).getChatServiceInstance();
        chatService.addChatListener(this);

        conversationManager = ApplicationManager.getApplication().getService(ConversationManager.class);

        splitter = new OnePixelSplitter(true, .98f);
        splitter.setDividerWidth(1);

        ActionListener submitAction = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                conversationManager.addMessage("user", textField.getText());
                chatService.sendMessage(conversationManager.toString(), textField.getText());
            }
        };

        textField = new ExpandableTextField();
        textField.addActionListener(submitAction);
        JButton sendButton = new JButton("Send");
        sendButton.addActionListener(submitAction);

        textField.registerKeyboardAction(submitAction, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, CTRL_DOWN_MASK), JComponent.WHEN_FOCUSED);

        actionPanel = new JPanel(new BorderLayout());
        actionPanel.add(textField, BorderLayout.CENTER);
        actionPanel.add(sendButton, BorderLayout.EAST);

        progressBar = new JProgressBar();
        progressBar.setVisible(false);
        contentPanel = new MessageGroupComponent();
        contentPanel.add(progressBar, BorderLayout.SOUTH);
        contentPanel.refresh();
        splitter.setFirstComponent(contentPanel);
        splitter.setSecondComponent(actionPanel);
    }

    /**
     * Return the content panel for plugin tool window factory
     *
     * @return the content panel
     */
    public JPanel getContentPanel() {
        return splitter;
    }

    /**
     * Add a chat message to the content panel
     * This method is called when the user sends a message or LLM give response to user.
     *
     * @param message the message to add
     * @param isUser  whether the message is from the user or not
     */
    public void addChatMessage(String message, boolean isUser) {
        MessageComponent messageComponent = new MessageComponent(message, isUser);
        answer = new MessageComponent("Fetching answer...", false);
        contentPanel.add(messageComponent);
        contentPanel.add(answer);
    }

    /**
     * @param event The event that is fired when the user sends a message
     */
    @Override
    public void sending(ChatEvent.Sending event) {
        waitForResponse(true);
        textField.setText("");
        addChatMessage(event.getMessage(), true);
    }

    /**
     * @param event The event that is fired when the message fails to send
     */
    @Override
    public void failed(ChatEvent.Failed event) {
        waitForResponse(false);
        answer.setText(event.getCause().getMessage());

    }

    /**
     * @param event The event that is fired when the response arrives
     */
    @Override
    public void responseArrived(ChatEvent.ResponseArrived event) {
        waitForResponse(false);
        conversationManager.addMessage("assistant", event.getMessage());
        answer.setText(event.getMessage());
    }

    /**
     * Set ui to wait for response
     *
     * @param status The status of the progress bar
     */
    public void waitForResponse(boolean status) {
        progressBar.setIndeterminate(status);
        progressBar.setVisible(status);
        actionPanel.revalidate();
        actionPanel.repaint();
    }
}
