package com.gitlab.team21.codearoma.smell;

import com.gitlab.team21.codearoma.llm.AskLLMComponent;
import com.gitlab.team21.codearoma.llm.CodeInspectionType;
import com.gitlab.team21.codearoma.settings.AppSettingsState;
import com.gitlab.team21.codearoma.settings.CodeSmell;
import com.intellij.codeInspection.AbstractBaseJavaLocalInspectionTool;
import com.intellij.codeInspection.ProblemsHolder;
import com.intellij.psi.*;
import org.jetbrains.annotations.NotNull;

/**
 * Inspection that checks if a class shows signs of primitive obsession
 */
public class PrimitiveObsessionInspection extends AbstractBaseJavaLocalInspectionTool {

    private static final String CODE_SMELL_NAME = CodeSmell.PRIMITIVE_OBSESSION.label;
    private static final String PRIMITIVE_LIMIT_MESSAGE = "Maximum number of primitives:";

    public @NotNull PsiElementVisitor buildVisitor(@NotNull final ProblemsHolder holder, boolean isOnTheFly) {
        return new JavaElementVisitor() {
            /**
             * This method is overridden to provide a custom visitor that inspects
             * method declarations with more than 5 primitive fields. IF these criteria are
             * met, register the problem in the ProblemsHolder.
             *
             * @param aClass The class to be evaluated.
             */
            @Override
            public void visitClass(@NotNull PsiClass aClass) {
                AppSettingsState settings = AppSettingsState.getInstance();
                // If the Primitive Obsession code smell detection feature has been toggled off, don't run the inspection code
                if (!settings.codeSmellStatus.get(CODE_SMELL_NAME))
                    return;

                int maximumNumberOfPrimitives = Integer.parseInt(settings.codeSmellLimits.get(CODE_SMELL_NAME).getLimits().get(PRIMITIVE_LIMIT_MESSAGE));
                super.visitClass(aClass);


                int count = 0;
                for (PsiField field : aClass.getFields()) {
                    if (isPrimitive(field)) {
                        if (++count > maximumNumberOfPrimitives) {
                            PsiElement highlight = aClass;
                            if (aClass.getNameIdentifier() != null) {
                                highlight = aClass.getNameIdentifier();
                            }
                            AskLLMComponent askLLM = new AskLLMComponent(CodeInspectionType.PrimitiveObsessionInspection);
                            askLLM.setCodeSnippet(aClass.getText());
                            askLLM.setHighlightedProblem("too many primitives");
                            holder.registerProblem(highlight,
                                    InspectionBundle.message(
                                            "inspection.primitive.obsession.problem.descriptor"), askLLM);
                            return;
                        }
                    }
                }
            }

            /**
             * Method to check if a field represent a primitive type
             * @param field to be checked
             * @return true if the field is a primitive, false otherwise
             */
            private boolean isPrimitive(PsiField field) {
                PsiType type = field.getType();
                return (type.equals(PsiType.INT) ||
                        type.equals(PsiType.CHAR) ||
                        type.equals(PsiType.BOOLEAN) ||
                        type.equals(PsiType.FLOAT) ||
                        type.equals(PsiType.SHORT) ||
                        type.equals(PsiType.BYTE) ||
                        type.equals(PsiType.LONG) ||
                        type.equals(PsiType.DOUBLE));
            }
        };
    }
}
