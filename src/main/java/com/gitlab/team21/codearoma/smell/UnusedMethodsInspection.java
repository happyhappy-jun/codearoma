package com.gitlab.team21.codearoma.smell;

import com.gitlab.team21.codearoma.llm.AskLLMComponent;
import com.gitlab.team21.codearoma.llm.CodeInspectionType;
import com.gitlab.team21.codearoma.settings.AppSettingsState;
import com.gitlab.team21.codearoma.settings.CodeSmell;
import com.intellij.codeInspection.AbstractBaseJavaLocalInspectionTool;
import com.intellij.codeInspection.ProblemsHolder;
import com.intellij.psi.JavaElementVisitor;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.PsiModifier;
import com.intellij.psi.search.searches.ReferencesSearch;
import org.jetbrains.annotations.NotNull;

/**
 * Implements an inspection to detect unused fields.
 */
final class UnusedMethodsInspection extends AbstractBaseJavaLocalInspectionTool {
    public @NotNull PsiElementVisitor buildVisitor(@NotNull final ProblemsHolder holder,
                                                   boolean isOnTheFly) {
        return new JavaElementVisitor() {
            /**
             * This method is overridden to provide a custom visitor that inspects
             * unused fields in the class. IF these criteria are
             * met, register the problem in the ProblemsHolder.
             *
             * @param method The method to be evaluated.
             */
            @Override
            public void visitMethod(@NotNull PsiMethod method) {
                super.visitMethod(method);
                if (!AppSettingsState.getInstance().codeSmellStatus.get(
                        CodeSmell.UNUSED_METHODS.label))
                    return;

                // ignore main method
                if (method.getName().equals("main") || method.getName().equals("Main")) {
                    return;
                }

                if (method.getIdentifyingElement() != null
                        && !method.hasModifierProperty(PsiModifier.ABSTRACT)
                        && !method.isConstructor()
                        && ReferencesSearch.search(method).findFirst() == null) {
                    AskLLMComponent askLLM = new AskLLMComponent(CodeInspectionType.UnusedMethodsInspection);
                    askLLM.setCodeSnippet(method.getText());
                    askLLM.setHighlightedProblem(method.getIdentifyingElement().getText());
                    holder.registerProblem(method.getIdentifyingElement(), InspectionBundle.message(
                            "inspection.unused.methods.problem.descriptor"), askLLM);
                }
            }
        };
    }
}
