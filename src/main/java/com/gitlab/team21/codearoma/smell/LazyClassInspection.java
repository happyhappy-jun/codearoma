package com.gitlab.team21.codearoma.smell;

import com.gitlab.team21.codearoma.llm.AskLLMComponent;
import com.gitlab.team21.codearoma.llm.CodeInspectionType;
import com.gitlab.team21.codearoma.settings.AppSettingsState;
import com.gitlab.team21.codearoma.settings.CodeSmell;
import com.intellij.codeInspection.AbstractBaseJavaLocalInspectionTool;
import com.intellij.codeInspection.ProblemsHolder;
import com.intellij.psi.*;
import com.intellij.psi.search.searches.ClassInheritorsSearch;
import com.intellij.psi.search.searches.MethodReferencesSearch;
import com.intellij.psi.search.searches.ReferencesSearch;
import com.intellij.util.Query;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.Objects;

/**
 * Implements an inspection to detect the class that is not instantiated or inherited.
 */
public class LazyClassInspection extends AbstractBaseJavaLocalInspectionTool {

    public @NotNull PsiElementVisitor buildVisitor(@NotNull final ProblemsHolder holder,
                                                   boolean isOnTheFly) {
        return new JavaElementVisitor() {

            /**
             * Evaluate PsiClass to see if they are unused.
             * For Abstract class and interface, they are not extended or implemented.
             * For normal class, they are not instantiated and don't have any references.
             * IF these criteria are met, register the problem in the ProblemsHolder.
             *
             * @param aClass The class to be evaluated.
             */
            @Override
            public void visitClass(@NotNull PsiClass aClass) {
                if (!AppSettingsState.getInstance().codeSmellStatus.get(CodeSmell.LAZY_CLASS.label))
                    return;

                super.visitClass(aClass);

                if (aClass.hasModifierProperty(PsiModifier.ABSTRACT) && !aClass.isInterface()) {
                    if (!hasInheritor(aClass)) {
                        AskLLMComponent askLLM = new AskLLMComponent(CodeInspectionType.LazyClassInspection);
                        askLLM.setCodeSnippet(aClass.getText());
                        askLLM.setHighlightedProblem(getClassName(aClass).getText());
                        holder.registerProblem(Objects.requireNonNull(getClassName(aClass)),
                                InspectionBundle.message(
                                        "inspection.lazy.class.abstract.display.descriptor"), askLLM);
                    }
                } else if (aClass.isInterface()) {
                    if (!hasInheritor(aClass)) {
                        AskLLMComponent askLLM = new AskLLMComponent(CodeInspectionType.LazyClassInspection);
                        askLLM.setCodeSnippet(aClass.getText());
                        askLLM.setHighlightedProblem(getClassName(aClass).getText());
                        holder.registerProblem(Objects.requireNonNull(getClassName(aClass)),
                                InspectionBundle.message(
                                        "inspection.lazy.class.interface.display.descriptor"), askLLM);
                    }
                } else if (!hasInstance(aClass)) {
                    AskLLMComponent askLLM = new AskLLMComponent(CodeInspectionType.LazyClassInspection);
                    askLLM.setCodeSnippet(aClass.getText());
                    askLLM.setHighlightedProblem(getClassName(aClass).getText());
                    holder.registerProblem(Objects.requireNonNull(getClassName(aClass)),
                            InspectionBundle.message(
                                    "inspection.lazy.class.general.display.descriptor"), askLLM);
                }
            }

            private PsiElement getClassName(PsiClass aClass) {
                if (aClass.getNameIdentifier() != null) {
                    return aClass.getNameIdentifier();
                } else {
                    return aClass.getOriginalElement();
                }
            }

            private boolean hasInheritor(PsiClass aClass) {
                Collection<PsiClass> inheritors = ClassInheritorsSearch.search(aClass).findAll();
                return !inheritors.isEmpty();
            }

            private boolean hasInstance(PsiClass aClass) {
                PsiMethod[] constructors = aClass.getConstructors();
                return isConstructorCalled(constructors) || isDefaultConstructorCalled(aClass);
            }

            private boolean isConstructorCalled(PsiMethod[] constructors) {
                for (PsiMethod constructor : constructors) {
                    Collection<PsiReference> constructorRefs = MethodReferencesSearch.search(
                            constructor).findAll();
                    if (!constructorRefs.isEmpty()) {
                        return true;
                    }
                }
                return false;
            }

            private boolean isDefaultConstructorCalled(PsiClass aClass) {
                Query<PsiReference> query = ReferencesSearch.search(aClass, aClass.getUseScope());
                return query.anyMatch(
                        ref -> ref != null && ref.getElement().getParent() instanceof PsiNewExpression);
            }
        };
    }
}
