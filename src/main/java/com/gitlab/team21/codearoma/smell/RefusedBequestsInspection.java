package com.gitlab.team21.codearoma.smell;

import com.gitlab.team21.codearoma.llm.AskLLMComponent;
import com.gitlab.team21.codearoma.llm.CodeInspectionType;
import com.gitlab.team21.codearoma.settings.AppSettingsState;
import com.gitlab.team21.codearoma.settings.CodeSmell;
import com.intellij.codeInspection.AbstractBaseJavaLocalInspectionTool;
import com.intellij.codeInspection.ProblemsHolder;
import com.intellij.psi.*;
import org.jetbrains.annotations.NotNull;

/**
 * Inspects the code to detect when a class is refusing bequests.
 */
public class RefusedBequestsInspection extends AbstractBaseJavaLocalInspectionTool {
    public @NotNull PsiElementVisitor buildVisitor(@NotNull final ProblemsHolder holder,
                                                   boolean isOnTheFly) {
        return new JavaElementVisitor() {
            @Override
            public void visitClass(@NotNull PsiClass aClass) {
                super.visitClass(aClass);
                if (!AppSettingsState.getInstance().codeSmellStatus.get(
                        CodeSmell.REFUSED_BEQUEST.label))
                    return;

                checkClassInheritance(aClass, holder);
            }

            void checkClassInheritance(@NotNull PsiClass aClass, ProblemsHolder holder) {
                PsiReferenceList referenceList = aClass.getExtendsList();

                if (referenceList != null && referenceList.getReferenceElements().length > 0) {

                    for (PsiMethod method : aClass.getMethods()) {
                        PsiCodeBlock codeBlock = method.getBody();
                        if (codeBlock != null && codeBlock.getStatements().length == 0
                                && method.getIdentifyingElement() != null) {
                            AskLLMComponent askLLM = new AskLLMComponent(CodeInspectionType.RefusedBequestsInspection);
                            askLLM.setCodeSnippet(aClass.getText());
                            askLLM.setHighlightedProblem(method.getText());
                            holder.registerProblem(method.getIdentifyingElement(),
                                    InspectionBundle.message("inspection.refused.bequests.problem.descriptor"), askLLM);
                        }
                    }
                }
            }
        };
    }
}
