package com.gitlab.team21.codearoma.smell;

import com.gitlab.team21.codearoma.llm.AskLLMComponent;
import com.gitlab.team21.codearoma.llm.CodeInspectionType;
import com.gitlab.team21.codearoma.settings.AppSettingsState;
import com.gitlab.team21.codearoma.settings.CodeSmell;
import com.intellij.codeInspection.AbstractBaseJavaLocalInspectionTool;
import com.intellij.codeInspection.ProblemsHolder;
import com.intellij.psi.*;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Implements an inspection to detect when field is public or using data class instead of record.
 */
final class DataClassInspection extends AbstractBaseJavaLocalInspectionTool {

    public @NotNull PsiElementVisitor buildVisitor(@NotNull final ProblemsHolder holder,
                                                   boolean isOnTheFly) {
        return new JavaElementVisitor() {

            /**
             * Evaluate PsiClass to see if they are data class.
             * If the class has only methods of constructor, getter, setter,
             * register the problem in the ProblemsHolder.
             *
             * @param aClass The class to be evaluated.
             */
            @Override
            public void visitClass(@NotNull PsiClass aClass) {
                super.visitClass(aClass);
                if (!AppSettingsState.getInstance().codeSmellStatus.get(CodeSmell.DATA_CLASS.label))
                    return;

                if (aClass.isRecord()) return;

                Set<String> methodNames = Arrays
                        .stream(aClass.getChildren())
                        .filter(child -> child instanceof PsiMethod)
                        .map(child -> ((PsiMethod) child).getName())
                        .collect(Collectors.toSet());

                methodNames.remove(aClass.getName());
                methodNames.removeAll(getters(aClass));

                if (methodNames.isEmpty()) {
                    AskLLMComponent askLLM = new AskLLMComponent(CodeInspectionType.DataClassInspection);
                    askLLM.setCodeSnippet(aClass.getText());
                    askLLM.setHighlightedProblem(getClassName(aClass).getText());
                    holder.registerProblem(Objects.requireNonNull(getClassName(aClass)),
                            InspectionBundle.message(
                                    "inspection.data.class.descriptor"), askLLM);
                }
            }

            /**
             * Evaluate PsiField to see if they are data class. If the field has public
             * annotation, register the problem in the ProblemsHolder.
             *
             * @param field The field to be evaluated.
             */
            @Override
            public void visitField(@NotNull PsiField field) {
                super.visitField(field);
                if (!AppSettingsState.getInstance().codeSmellStatus.get("Data Class")) {
                    return;
                }

                if (field.hasModifierProperty(PsiModifier.PUBLIC)) {
                    AskLLMComponent askLLM = new AskLLMComponent(CodeInspectionType.DataClassInspection);
                    askLLM.setCodeSnippet(field.getContainingClass().getText());
                    askLLM.setHighlightedProblem(field.getText());
                    holder.registerProblem(field,
                            InspectionBundle.message(
                                    "inspection.data.class.public.field.descriptor"), askLLM);
                }
            }

            private Set<String> getters(PsiClass aClass) {
                Set<String> getters = new HashSet<>();
                PsiField[] fields = Arrays
                        .stream(aClass.getChildren())
                        .filter(child -> child instanceof PsiField)
                        .toArray(PsiField[]::new);

                for (PsiField field : fields) {
                    String fieldName = field.getName();
                    getters.add(fieldName);
                    getters.add("get" + fieldName);
                    getters.add("get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1));
                }
                return getters;
            }

            private PsiElement getClassName(PsiClass aClass) {
                if (aClass.getNameIdentifier() != null) {
                    return aClass.getNameIdentifier();
                } else {
                    return aClass.getOriginalElement();
                }
            }
        };
    }
}
