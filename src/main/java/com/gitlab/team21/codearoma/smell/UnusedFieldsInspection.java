package com.gitlab.team21.codearoma.smell;

import com.gitlab.team21.codearoma.llm.AskLLMComponent;
import com.gitlab.team21.codearoma.llm.CodeInspectionType;
import com.gitlab.team21.codearoma.settings.AppSettingsState;
import com.gitlab.team21.codearoma.settings.CodeSmell;
import com.intellij.codeInspection.AbstractBaseJavaLocalInspectionTool;
import com.intellij.codeInspection.ProblemsHolder;
import com.intellij.psi.JavaElementVisitor;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.PsiField;
import com.intellij.psi.search.searches.ReferencesSearch;
import org.jetbrains.annotations.NotNull;

/**
 * Implements an inspection to detect unused fields.
 */
final class UnusedFieldsInspection extends AbstractBaseJavaLocalInspectionTool {

    public @NotNull PsiElementVisitor buildVisitor(@NotNull final ProblemsHolder holder,
                                                   boolean isOnTheFly) {
        return new JavaElementVisitor() {
            /**
             * This method is overridden to provide a custom visitor that inspects
             * unused fields in the class. IF these criteria are
             * met, register the problem in the ProblemsHolder.
             *
             * @param aClass The class to be evaluated.
             */
            @Override
            public void visitClass(@NotNull PsiClass aClass) {
                super.visitClass(aClass);
                if (!AppSettingsState.getInstance().codeSmellStatus.get(
                        CodeSmell.UNUSED_FIELDS.label))
                    return;

                PsiField[] fields = aClass.getFields();

                for (PsiField field : fields) {
                    if (ReferencesSearch.search(field).findFirst() == null) {
                        AskLLMComponent askLLM = new AskLLMComponent(CodeInspectionType.UnusedFieldsInspection);
                        askLLM.setCodeSnippet(aClass.getText());
                        askLLM.setHighlightedProblem(field.getName());
                        holder.registerProblem(field, InspectionBundle.message(
                                "inspection.unused.fields.problem.descriptor"), askLLM);
                    }
                }
            }
        };
    }
}
