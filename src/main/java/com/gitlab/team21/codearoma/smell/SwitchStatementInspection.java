package com.gitlab.team21.codearoma.smell;

import com.gitlab.team21.codearoma.llm.AskLLMComponent;
import com.gitlab.team21.codearoma.llm.CodeInspectionType;
import com.gitlab.team21.codearoma.settings.AppSettingsState;
import com.gitlab.team21.codearoma.settings.CodeSmell;
import com.intellij.codeInspection.AbstractBaseJavaLocalInspectionTool;
import com.intellij.codeInspection.ProblemsHolder;
import com.intellij.psi.*;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;

/**
 * Implements an inspection to detect when a switch statement has too many cases.
 */
final class SwitchStatementInspection extends AbstractBaseJavaLocalInspectionTool {

    private static final String CODE_SMELL_NAME = CodeSmell.SWITCH_STATEMENT.label;
    private static final String CASE_LIMIT_MESSAGE = "Maximum number of cases:";

    public @NotNull PsiElementVisitor buildVisitor(@NotNull final ProblemsHolder holder,
                                                   boolean isOnTheFly) {
        return new JavaElementVisitor() {
            @Override
            public void visitSwitchStatement(@NotNull PsiSwitchStatement switchStatement) {
                AppSettingsState settings = AppSettingsState.getInstance();
                // If the Switch statement code smell detection feature has been toggled off, don't run the inspection code
                if (!settings.codeSmellStatus.get(CODE_SMELL_NAME))
                    return;

                int maximumNumberOfCases = Integer.parseInt(settings.codeSmellLimits.get(CODE_SMELL_NAME).getLimits().get(CASE_LIMIT_MESSAGE));
                super.visitSwitchStatement(switchStatement);

                int caseCount = countCases(switchStatement);
                if (caseCount > maximumNumberOfCases) {
                    AskLLMComponent askLLM = new AskLLMComponent(CodeInspectionType.SwitchStatementInspection);
                    askLLM.setCodeSnippet(switchStatement.getText());
                    askLLM.setHighlightedProblem("too many switch cases");
                    holder.registerProblem(switchStatement,
                            "Switch statement has too many cases (" + caseCount + "), consider refactoring.", askLLM);
                }
            }

            private int countCases(PsiSwitchStatement switchStatement) {
                if (switchStatement.getBody() == null) {
                    return 0;
                }

                return (int) Arrays.stream(switchStatement.getChildren())
                        .filter(child -> child instanceof PsiCodeBlock)
                        .flatMap(child -> Arrays.stream(((PsiCodeBlock) child).getStatements()))
                        .filter(st -> st instanceof PsiSwitchLabelStatement)
                        .count();

            }
        };
    }
}
