package com.gitlab.team21.codearoma.smell;

import com.gitlab.team21.codearoma.llm.AskLLMComponent;
import com.gitlab.team21.codearoma.llm.CodeInspectionType;
import com.gitlab.team21.codearoma.settings.AppSettingsState;
import com.gitlab.team21.codearoma.settings.CodeSmell;
import com.intellij.codeInspection.AbstractBaseJavaLocalInspectionTool;
import com.intellij.codeInspection.ProblemsHolder;
import com.intellij.psi.JavaElementVisitor;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.PsiMethod;
import org.jetbrains.annotations.NotNull;

/**
 * Implements an inspection to detect when a method has more than 3 parameters.
 */
final class LongParameterListInspection extends AbstractBaseJavaLocalInspectionTool {

    private static final String CODE_SMELL_NAME = CodeSmell.LONG_PARAMETER_LIST.label;
    private static final String PARAMETER_LIMIT_MESSAGE = "Maximum number of parameters:";

    public @NotNull PsiElementVisitor buildVisitor(@NotNull final ProblemsHolder holder, boolean isOnTheFly) {
        return new JavaElementVisitor() {
            /**
             * This method is overridden to provide a custom visitor that inspects
             * method declarations with more than 3 parameters. IF these criteria are
             * met, register the problem in the ProblemsHolder.
             *
             * @param method The method to be evaluated.
             */
            @Override
            public void visitMethod(@NotNull PsiMethod method) {
                AppSettingsState settings = AppSettingsState.getInstance();
                // If the Long Parameter List code smell detection feature has been toggled off, don't run the inspection code
                if (!settings.codeSmellStatus.get(CODE_SMELL_NAME))
                    return;

                int maximumNumberOfParameters = Integer.parseInt(settings.codeSmellLimits.get(CODE_SMELL_NAME).getLimits().get(PARAMETER_LIMIT_MESSAGE));
                super.visitMethod(method);

                if (method.getParameterList().getParametersCount() > maximumNumberOfParameters && method.getIdentifyingElement() != null) {
                    AskLLMComponent askLLM = new AskLLMComponent(CodeInspectionType.LongParameterListInspection);
                    askLLM.setCodeSnippet(method.getText());
                    askLLM.setHighlightedProblem(method.getParameterList().getText());
                    holder.registerProblem(method.getIdentifyingElement(), InspectionBundle.message("inspection.long.parameter.list.problem.descriptor"), askLLM);
                }
            }
        };
    }
}
