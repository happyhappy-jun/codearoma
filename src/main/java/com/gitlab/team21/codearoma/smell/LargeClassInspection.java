package com.gitlab.team21.codearoma.smell;

import com.gitlab.team21.codearoma.llm.AskLLMComponent;
import com.gitlab.team21.codearoma.llm.CodeInspectionType;
import com.gitlab.team21.codearoma.settings.AppSettingsState;
import com.gitlab.team21.codearoma.settings.CodeSmell;
import com.intellij.codeInspection.AbstractBaseJavaLocalInspectionTool;
import com.intellij.codeInspection.ProblemsHolder;
import com.intellij.psi.JavaElementVisitor;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import org.jetbrains.annotations.NotNull;

/**
 * Implement an inspection to detect large classes
 */
class LargeClassInspection extends AbstractBaseJavaLocalInspectionTool {

    private static final String CODE_SMELL_NAME = CodeSmell.LARGE_CLASS.label;
    private static final String LINE_LIMIT_MESSAGE = "Maximum number of lines:";
    private static final String FIELD_LIMIT_MESSAGE = "Maximum number of fields:";
    private static final String METHOD_LIMIT_MESSAGE = "Maximum number of methods:";

    public @NotNull PsiElementVisitor buildVisitor(@NotNull final ProblemsHolder holder,
                                                   boolean isOnTheFly) {
        return new JavaElementVisitor() {
            /**
             * This method is overridden to provide a custom visitor that inspects
             * method declarations with more than 10 fields or more than 10 methods or
             * the class is written in more than 200 lines. IF these criteria are
             * met, register the problem in the ProblemsHolder.
             *
             * @param aClass The class to be evaluated.
             */
            @Override
            public void visitClass(@NotNull PsiClass aClass) {
                AppSettingsState settings = AppSettingsState.getInstance();
                // If the Large Class code smell detection feature has been toggled off, don't run the inspection code
                if (!settings.codeSmellStatus.get(CODE_SMELL_NAME))
                    return;

                int maximumNumberOfLines = Integer.parseInt(settings.codeSmellLimits.get(CODE_SMELL_NAME).getLimits().get(LINE_LIMIT_MESSAGE));
                int maximumNumberOfFields = Integer.parseInt(settings.codeSmellLimits.get(CODE_SMELL_NAME).getLimits().get(FIELD_LIMIT_MESSAGE));
                int maximumNumberOfMethods = Integer.parseInt(settings.codeSmellLimits.get(CODE_SMELL_NAME).getLimits().get(METHOD_LIMIT_MESSAGE));
                super.visitClass(aClass);


                if (aClass.getMethods().length > maximumNumberOfMethods ||
                        aClass.getFields().length > maximumNumberOfFields ||
                        aClass.getText().lines().count() > maximumNumberOfLines) {
                    PsiElement highlight = aClass;
                    if (aClass.getNameIdentifier() != null) {
                        highlight = aClass.getNameIdentifier();
                    }

                    AskLLMComponent askLLM = new AskLLMComponent(CodeInspectionType.LargeClassInspection);
                    askLLM.setCodeSnippet(aClass.getText());
                    askLLM.setHighlightedProblem(highlight.getText());
                    holder.registerProblem(highlight,
                            InspectionBundle.message(
                                    "inspection.large.class.problem.descriptor"), askLLM);
                }
            }
        };
    }
}
