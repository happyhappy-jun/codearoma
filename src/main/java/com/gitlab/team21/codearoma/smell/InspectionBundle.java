package com.gitlab.team21.codearoma.smell;

import com.intellij.DynamicBundle;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.PropertyKey;

/**
 * Bundle for inspection messages
 * This class help retrieve messages from the inspection bundle
 */
public final class InspectionBundle extends DynamicBundle {

    @NonNls
    public static final String BUNDLE = "messages.InspectionBundle";
    private static final InspectionBundle ourInstance = new InspectionBundle();

    private InspectionBundle() {
        super(BUNDLE);
    }

    public static @Nls String message(@NotNull @PropertyKey(resourceBundle = BUNDLE) String key,
                                      Object @NotNull ... params) {
        return ourInstance.getMessage(key, params);
    }

}
