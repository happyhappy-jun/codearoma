package com.gitlab.team21.codearoma.smell;

import com.gitlab.team21.codearoma.llm.AskLLMComponent;
import com.gitlab.team21.codearoma.llm.CodeInspectionType;
import com.gitlab.team21.codearoma.settings.AppSettingsState;
import com.gitlab.team21.codearoma.settings.CodeSmell;
import com.intellij.codeInspection.AbstractBaseJavaLocalInspectionTool;
import com.intellij.codeInspection.ProblemsHolder;
import com.intellij.psi.JavaElementVisitor;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.PsiStatement;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;

/**
 * Implements an inspection to detect duplicated codes in constructor between superclass and subclass.
 */
final class DuplicatedConstructorInspection extends AbstractBaseJavaLocalInspectionTool {
    public @NotNull PsiElementVisitor buildVisitor(@NotNull final ProblemsHolder holder,
                                                   boolean isOnTheFly) {
        return new JavaElementVisitor() {
            /**
             * This method is overridden to provide a custom visitor that inspects
             * unused fields in the class. IF these criteria are
             * met, register the problem in the ProblemsHolder.
             *
             * @param method The method to be evaluated.
             */

            @Override
            public void visitMethod(@NotNull PsiMethod method) {
                super.visitElement(method);
                if (!AppSettingsState.getInstance().codeSmellStatus.get(
                        CodeSmell.DUPLICATED_CONSTRUCTOR.label))
                    return;

                if (method.isConstructor() && method.getContainingClass() != null
                        && method.getContainingClass().getSuperClass() != null) {
                    var superConstructor = Arrays.stream(method.getContainingClass().getSuperClass().getConstructors()).findFirst();

                    if (method.getBody() != null && superConstructor.isPresent() && superConstructor.get().getBody() != null) {
                        for (PsiStatement statement : method.getBody().getStatements()) {
                            if (superConstructor.get().getBody().getText().contains(statement.getText())) {
                                AskLLMComponent askLLM = new AskLLMComponent(CodeInspectionType.DuplicatedConstructorInspection);
                                askLLM.setCodeSnippet(method.getContainingClass().getText() + "\n\n" + superConstructor.get().getContainingClass().getText());
                                askLLM.setHighlightedProblem(statement.getText());
                                holder.registerProblem(statement, InspectionBundle.message(
                                        "inspection.duplicated.constructor.display.descriptor"), askLLM);
                            }
                        }
                    }
                }
            }

        };
    }
}
