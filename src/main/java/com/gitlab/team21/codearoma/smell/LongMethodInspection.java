package com.gitlab.team21.codearoma.smell;

import com.gitlab.team21.codearoma.llm.AskLLMComponent;
import com.gitlab.team21.codearoma.llm.CodeInspectionType;
import com.gitlab.team21.codearoma.settings.AppSettingsState;
import com.gitlab.team21.codearoma.settings.CodeSmell;
import com.intellij.codeInspection.AbstractBaseJavaLocalInspectionTool;
import com.intellij.codeInspection.ProblemsHolder;
import com.intellij.psi.JavaElementVisitor;
import com.intellij.psi.PsiCodeBlock;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.PsiMethod;
import org.jetbrains.annotations.NotNull;

/**
 * Implement an inspection to detect when a method is too long
 */
public class LongMethodInspection extends AbstractBaseJavaLocalInspectionTool {

    private static final String CODE_SMELL_NAME = CodeSmell.LONG_METHOD.label;
    private static final String LINE_LIMIT_MESSAGE = "Maximum number of lines:";

    public @NotNull PsiElementVisitor buildVisitor(@NotNull final ProblemsHolder holder, boolean isOnTheFly) {
        return new JavaElementVisitor() {
            /**
             * This method is overridden to provide a custom visitor that inspects
             * method declarations with more than 10 lines. IF these criteria are
             * met, register the problem in the ProblemsHolder.
             *
             * @param method The method to be evaluated.
             */
            @Override
            public void visitMethod(@NotNull PsiMethod method) {
                // If the Long Method code smell detection feature has been toggled off, don't run the inspection code
                AppSettingsState settings = AppSettingsState.getInstance();
                if (!settings.codeSmellStatus.get(CODE_SMELL_NAME))
                    return;

                int maximumNumberOfLines = Integer.parseInt(settings.codeSmellLimits.get(CODE_SMELL_NAME).getLimits().get(LINE_LIMIT_MESSAGE));
                super.visitMethod(method);
                PsiCodeBlock body = method.getBody();

                if (!(body == null || (body.getText()) == null)) {
                    if (body.getText().lines().count() > maximumNumberOfLines) {
                        AskLLMComponent askLLM = new AskLLMComponent(CodeInspectionType.LongMethodInspection);
                        askLLM.setCodeSnippet(method.getText());
                        askLLM.setHighlightedProblem(method.getName());
                        holder.registerProblem(method,
                                InspectionBundle.message(
                                        "inspection.long.method.problem.descriptor"), askLLM);
                    }
                }
            }
        };
    }
}
