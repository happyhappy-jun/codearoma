package com.gitlab.team21.codearoma.smell;

import com.gitlab.team21.codearoma.llm.AskLLMComponent;
import com.gitlab.team21.codearoma.llm.CodeInspectionType;
import com.gitlab.team21.codearoma.settings.AppSettingsState;
import com.gitlab.team21.codearoma.settings.CodeSmell;
import com.intellij.codeInspection.AbstractBaseJavaLocalInspectionTool;
import com.intellij.codeInspection.ProblemsHolder;
import com.intellij.psi.*;
import com.intellij.psi.util.PsiTreeUtil;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Implements an inspection to detect unused parameters.
 */
final class UnusedParametersInspection extends AbstractBaseJavaLocalInspectionTool {
    public @NotNull PsiElementVisitor buildVisitor(@NotNull final ProblemsHolder holder,
                                                   boolean isOnTheFly) {
        return new JavaElementVisitor() {
            /**
             * This method is overridden to provide a custom visitor that inspects
             * unused parameters in the method. IF these criteria are
             * met, register the problem in the ProblemsHolder.
             *
             * @param method The method to be evaluated.
             */
            @Override
            public void visitMethod(@NotNull PsiMethod method) {
                super.visitMethod(method);
                if (!AppSettingsState.getInstance().codeSmellStatus.get(
                        CodeSmell.UNUSED_PARAMETERS.label)) {
                    return;
                }

                if (!method.hasParameters()) {
                    return;
                }

                PsiParameter[] parameters = method.getParameterList().getParameters();
                PsiCodeBlock body = method.getBody();

                if (body == null) {
                    return;
                }

                if (body.isEmpty()) {
                    for (PsiParameter parameter : parameters) {
                        AskLLMComponent askLLM = new AskLLMComponent(CodeInspectionType.UnusedParametersInspection);
                        askLLM.setCodeSnippet(method.getText());
                        askLLM.setHighlightedProblem(parameter.getText());
                        holder.registerProblem(parameter, InspectionBundle.message(
                                "inspection.unused.parameters.problem.descriptor"), askLLM);
                    }
                    return;
                }

                List<String> identifiers = PsiTreeUtil.findChildrenOfType(body,
                        PsiIdentifier.class).stream().map(PsiIdentifier::getText).toList();

                for (PsiParameter parameter : parameters) {
                    if (!identifiers.contains(parameter.getName())) {
                        AskLLMComponent askLLM = new AskLLMComponent(CodeInspectionType.UnusedParametersInspection);
                        askLLM.setCodeSnippet(method.getText());
                        askLLM.setHighlightedProblem(parameter.getText());
                        holder.registerProblem(parameter, InspectionBundle.message(
                                "inspection.unused.parameters.problem.descriptor"), askLLM);
                    }
                }
            }
        };
    }
}
