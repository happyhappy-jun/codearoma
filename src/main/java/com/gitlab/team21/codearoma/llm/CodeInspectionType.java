package com.gitlab.team21.codearoma.llm;


/**
 * Enum class for code inspection types.
 */
public enum CodeInspectionType {

    DuplicatedConstructorInspection(1, "duplicated constructor"),
    LargeClassInspection(2, "large class"),
    LazyClassInspection(3, "lazy class"),
    LongMethodInspection(4, "long method"),
    LongParameterListInspection(5, "long parameter"),
    PrimitiveObsessionInspection(6, "primitive obsession"),
    RefusedBequestsInspection(7, "refused bequest"),
    SwitchStatementInspection(8, "switch statement"),
    UnusedFieldsInspection(9, "unused field"),
    UnusedMethodsInspection(10, "unused method"),
    UnusedParametersInspection(11, "unused parameters"),
    DataClassInspection(12, "data class"),
    TemporaryFieldInspection(13, "temporary field");

    private final int id;
    private final String name;

    CodeInspectionType(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }
}

