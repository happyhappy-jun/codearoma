package com.gitlab.team21.codearoma.llm;

import com.knuddels.jtokkit.Encodings;
import com.knuddels.jtokkit.api.Encoding;
import com.knuddels.jtokkit.api.EncodingRegistry;
import com.knuddels.jtokkit.api.EncodingType;

import java.util.List;

/**
 * Tokenizes a string using the CL100K encoding.
 */
public class Tokenizer {
    EncodingRegistry registry;
    Encoding enc;

    public Tokenizer() {
        registry = Encodings.newDefaultEncodingRegistry();
        enc = registry.getEncoding(EncodingType.CL100K_BASE);
    }

    /**
     * @param input the string to be tokenized
     * @return the list of tokens
     */
    public List<Integer> encode(String input) {
        return enc.encode(input);
    }
}
