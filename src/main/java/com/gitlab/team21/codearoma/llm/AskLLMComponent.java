package com.gitlab.team21.codearoma.llm;

import com.gitlab.team21.codearoma.chat.ChatService;
import com.gitlab.team21.codearoma.chat.ChatServiceSingleton;
import com.gitlab.team21.codearoma.llm.model.ConversationManager;
import com.intellij.codeInspection.LocalQuickFix;
import com.intellij.codeInspection.ProblemDescriptor;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.project.Project;
import org.jetbrains.annotations.NotNull;

/**
 * The {@code AskLLMComponent} class provides functionality for asking a Language Learning Model (LLM)
 * about solutions to code smells identified during code inspection.
 * It implements the {@code LocalQuickFix} interface for integration with IntelliJ IDEA's inspection framework.
 */
public class AskLLMComponent implements LocalQuickFix {
    public final ChatService chatService;
    private final ConversationManager conversationManager;
    public CodeInspectionType codeInspection;
    private String codeSnippet;
    private String highlightedProblem;

    /**
     * Constructs a new {@code AskLLMComponent} with a specified code inspection type.
     * Initializes the chat service and conversation manager.
     *
     * @param codeInspection The type of code inspection being addressed.
     */
    public AskLLMComponent(CodeInspectionType codeInspection) {
        this.codeInspection = codeInspection;
        this.chatService = ApplicationManager.getApplication().getService(ChatServiceSingleton.class).getChatServiceInstance();
        this.conversationManager = ApplicationManager.getApplication().getService(ConversationManager.class);
    }

    /**
     * Constructs a new {@code AskLLMComponent} with a specified code inspection type and chat service.
     * Useful for dependency injection or testing purposes.
     *
     * @param codeInspection The type of code inspection being addressed.
     * @param chatService    The chat service to be used for communication.
     */
    public AskLLMComponent(CodeInspectionType codeInspection, ChatService chatService) {
        this.codeInspection = codeInspection;
        this.chatService = chatService;
        this.conversationManager = ApplicationManager.getApplication().getService(ConversationManager.class);
    }


    private String createQuery() {
        return "How can I fix the code smell of " + codeInspection.getName() + " \\\"" + getHighlightedProblem() + "\\\" in this code snippet?\n\n\\\"\n" + getCodeSnippet() + "\n\\\"";
    }


    /**
     * Gets the name of the fix action.
     *
     * @return A {@code String} representing the name of the fix action.
     */
    @NotNull
    @Override
    public String getName() {
        return "Ask LLM: " + codeInspection.getName();
    }

    /**
     * Gets the family name of the fix action. In this case, it's the same as the name.
     *
     * @return A {@code String} representing the family name of the fix action.
     */
    @NotNull
    @Override
    public String getFamilyName() {
        return getName();
    }


    /**
     * Applies the fix action to the problem described by the specified descriptor.
     * Sends a message to the LLM with the code snippet and highlighted problem.
     *
     * @param project    The project containing the problem.
     * @param descriptor The descriptor specifying the problem.
     */
    @Override
    public void applyFix(@NotNull Project project, @NotNull ProblemDescriptor descriptor) {
        conversationManager.addMessage("user", createQuery());
        chatService.sendMessage(conversationManager.toString(), createQuery());
    }


    /**
     * Gets the code snippet.
     *
     * @return A {@code String} representing the code snippet.
     */
    public String getCodeSnippet() {
        return codeSnippet;
    }


    /**
     * Sets the code snippet.
     *
     * @param codeSnippet A {@code String} representing the code snippet.
     */
    public void setCodeSnippet(String codeSnippet) {
        this.codeSnippet = codeSnippet;
    }


    /**
     * Gets the highlighted problem.
     *
     * @return A {@code String} representing the highlighted problem.
     */
    public String getHighlightedProblem() {
        return highlightedProblem;
    }


    /**
     * Sets the highlighted problem.
     *
     * @param highlightedProblem A {@code String} representing the highlighted problem.
     */
    public void setHighlightedProblem(String highlightedProblem) {
        this.highlightedProblem = highlightedProblem;
    }
}
