package com.gitlab.team21.codearoma.llm.model;

import java.io.Serializable;

/**
 * @param role    The role of the message (e.g. "user", "assistant", "system")
 * @param content The content of the message
 */
public record Message(String role, String content) implements Serializable {
}
