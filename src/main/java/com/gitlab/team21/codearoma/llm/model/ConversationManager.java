package com.gitlab.team21.codearoma.llm.model;

import com.gitlab.team21.codearoma.llm.Tokenizer;
import com.google.gson.Gson;

/**
 * Class to manage the conversation between user and LLM
 * It stores the context of the conversation and the total number of tokens
 * If the total number of tokens exceeds the limit, the oldest message will be removed
 */
public class ConversationManager {
    private static final Integer MAX_TOKENS = 2048;
    private final ConversationContext context;
    private final Tokenizer tokenizer;
    private Integer totalTokens;

    public ConversationManager() {
        this.context = new ConversationContext();
        this.totalTokens = 0;
        this.tokenizer = new Tokenizer();

        addMessage("system",
                "You are a Senior Java Programmer. From now on, your fellow programmers will ask you questions about Java, OOP, and Code Smell." +
                        " You will answer them to the best of your ability. You will not be able to answer all questions, but you will try your best." +
                        "You should respond in markdown format.");
    }

    /**
     * @return The context of the conversation
     */
    public ConversationContext getContext() {
        return context;
    }

    /**
     * @return The total number of tokens in the conversation
     */
    public Integer getTotalTokens() {
        return totalTokens;
    }


    /**
     * Adds a message to the conversation. If the total token count exceeds the maximum limit
     * after adding the message, the oldest message is removed to make space.
     *
     * @param role    The role of the message sender (e.g., "user", "system").
     * @param content The content of the message.
     */
    public void addMessage(String role, String content) {
        if (isFull(content)) {
            while (isFull(content)) {
                System.out.println("Removing oldest message");
                Message oldestMessage = context.getMessages().poll();
                assert oldestMessage != null;
                totalTokens -= tokenizer.encode(oldestMessage.content()).size();
            }
        }
        context.addMessage(role, content);
        totalTokens += tokenizer.encode(content).size();
    }

    /**
     * Determines if adding a new message would exceed the maximum token limit.
     *
     * @param content The content of the message to be added.
     * @return {@code true} if adding the message would exceed the token limit; {@code false} otherwise.
     */
    public boolean isFull(String content) {
        return totalTokens + tokenizer.encode(content).size() > MAX_TOKENS;
    }


    /**
     * Converts the current conversation context to a JSON string.
     *
     * @return A {@code String} representing the conversation context in JSON format.
     */
    public String toString() {
        return new Gson().toJson(context);
    }
}
