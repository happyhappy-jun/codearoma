package com.gitlab.team21.codearoma.llm;

import com.gitlab.team21.codearoma.exception.InvalidRequestException;
import com.gitlab.team21.codearoma.settings.AppSettingsState;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URI;

/**
 * Class to call the LLM API
 */
public class CallLLMApi {
    CloseableHttpClient httpClient;

    /* Default constructor */
    public CallLLMApi(HttpClient httpClient) {
        this.httpClient = (CloseableHttpClient) httpClient;
    }

    private static String extractContentFromResponse(String response) {
        JSONObject jsonResponse = new JSONObject(response);
        JSONArray choices = jsonResponse.getJSONArray("choices");
        if (!choices.isEmpty()) {
            JSONObject firstChoice = choices.getJSONObject(0);
            JSONObject messageObject = firstChoice.getJSONObject("message");
            return messageObject.getString("content");
        }
        return "";
    }

    /**
     * Call the LLM API
     *
     * @param context The context of the conversation
     * @return The response from LLM
     * @throws Exception
     */
    public String callApi(String context) throws Exception {

        String url = "https://api.openai.com/v1/chat/completions";
        String apiKey = AppSettingsState.getInstance().openAiApiKey;
        // Create the HTTP POST request
        HttpUriRequest post = RequestBuilder.post()
                .setUri(new URI(url))
                .addHeader("Authorization", "Bearer " + apiKey)
                .addHeader("Content-Type", "application/json")
                .setEntity(new StringEntity(context))
                .build();

        // Execute the request
        String responseString;
        try (CloseableHttpClient responseHttpClient = this.httpClient;
             CloseableHttpResponse response = responseHttpClient.execute(post)) {
            responseString = EntityUtils.toString(response.getEntity());
        }


        if (new JSONObject(responseString).keySet().stream().anyMatch("error"::equals)) {
            throw new InvalidRequestException("Invalid request\nCheck you properly add OpenAI key in Configuration\nGo to Settings > Tools > CodeAroma");
        }

        return extractContentFromResponse(responseString);
    }

    /**
     * Set the HTTP client
     *
     * @param httpClient The HTTP client to be set
     */
    public void setHttpClient(CloseableHttpClient httpClient) {
        this.httpClient = httpClient;
    }
}


