package com.gitlab.team21.codearoma.llm.model;

import java.io.Serializable;
import java.util.Queue;

/**
 * Class to store the context of a conversation
 */
public class ConversationContext implements Serializable {
    private final String model;
    private final Queue<Message> messages;
    private final Integer max_tokens;

    public ConversationContext() {
        this.model = "gpt-3.5-turbo";
        this.messages = new java.util.LinkedList<>();
        this.max_tokens = 2048;
    }

    /**
     * Return the list of message between user and LLM
     *
     * @return messages in the conversation
     */
    public Queue<Message> getMessages() {
        return messages;
    }

    /**
     * Add a message object to the conversation
     *
     * @param role    The role of the message sender (i.e. user, assistant)
     * @param content The content of the message
     */
    public void addMessage(String role, String content) {
        messages.add(new Message(role, content));
    }
}
