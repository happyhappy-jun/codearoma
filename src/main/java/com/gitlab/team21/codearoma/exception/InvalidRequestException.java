package com.gitlab.team21.codearoma.exception;

/**
 * Exception thrown when the request is invalid.
 */
public class InvalidRequestException extends Exception {
    public InvalidRequestException(String message) {
        super(message);
    }
}
