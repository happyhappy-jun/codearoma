package com.gitlab.team21.codearoma.settings;

/**
 * Enum class for code smells.
 */
public enum CodeSmell {
    DATA_CLASS("Data Class"),
    DUPLICATED_CONSTRUCTOR("Duplicated Constructor"),
    LARGE_CLASS("Large Class"),
    LAZY_CLASS("Lazy Class"),
    LONG_METHOD("Long Method"),
    LONG_PARAMETER_LIST("Long Parameter List"),
    PRIMITIVE_OBSESSION("Primitive Obsession"),
    REFUSED_BEQUEST("Refused Bequest"),
    SWITCH_STATEMENT("Switch Statement"),
    UNUSED_FIELDS("Unused Fields"),
    UNUSED_METHODS("Unused Methods"),
    UNUSED_PARAMETERS("Unused Parameters");

    public final String label;

    CodeSmell(String label) {
        this.label = label;
    }
}
