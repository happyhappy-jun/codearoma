package com.gitlab.team21.codearoma.settings;

import com.intellij.util.xmlb.annotations.MapAnnotation;
import com.intellij.util.xmlb.annotations.OptionTag;

import java.util.LinkedHashMap;

/**
 * Stores the limits for a code smell
 */
public class CodeSmellLimits {
    @OptionTag
    private final String smellName;
    @MapAnnotation(surroundWithTag = false, surroundKeyWithTag = false, surroundValueWithTag = false)
    private final LinkedHashMap<String, String> limitMap;

    /**
     * The default constructor is never used, but its existence is required for loadState() in
     * AppSettingsState.java to deserialize CodeSmellLimits objects properly
     **/
    public CodeSmellLimits() {
        this.smellName = "";
        this.limitMap = new LinkedHashMap<>();
    }

    public CodeSmellLimits(String smellName) {
        this.smellName = smellName;
        this.limitMap = new LinkedHashMap<>();
    }

    /**
     * @param limitMessage key for limit map
     * @param limit        value for limit map
     */
    public void updateLimit(String limitMessage, String limit) {
        limitMap.put(limitMessage, limit);
    }

    /**
     * @return the name of the code smell
     */
    public String getSmellName() {
        return smellName;
    }

    /**
     * @return the map of limits for the code smell
     */
    public LinkedHashMap<String, String> getLimits() {
        return limitMap;
    }
}
