package com.gitlab.team21.codearoma.settings;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import com.intellij.util.xmlb.XmlSerializerUtil;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedHashMap;

import static com.gitlab.team21.codearoma.settings.CodeSmell.*;

/**
 * This class is responsible for storing the settings of the plugin
 * The Model of MVC pattern
 */
@State(
        name = "com.gitlab.team21.codearoma.settings.AppSettingsState",
        storages = @Storage("SdkConfigurePlugin.xml")
)
final public class AppSettingsState implements PersistentStateComponent<AppSettingsState> {

    public LinkedHashMap<String, Boolean> codeSmellStatus = new LinkedHashMap<>();
    public LinkedHashMap<String, CodeSmellLimits> codeSmellLimits = new LinkedHashMap<>();
    public String openAiApiKey = "";

    public AppSettingsState() {
        codeSmellStatus.put(DATA_CLASS.label, true);
        codeSmellStatus.put(DUPLICATED_CONSTRUCTOR.label, true);
        codeSmellStatus.put(LARGE_CLASS.label, true);
        codeSmellStatus.put(LAZY_CLASS.label, true);
        codeSmellStatus.put(LONG_METHOD.label, true);
        codeSmellStatus.put(LONG_PARAMETER_LIST.label, true);
        codeSmellStatus.put(PRIMITIVE_OBSESSION.label, true);
        codeSmellStatus.put(REFUSED_BEQUEST.label, true);
        codeSmellStatus.put(SWITCH_STATEMENT.label, true);
        codeSmellStatus.put(UNUSED_FIELDS.label, true);
        codeSmellStatus.put(UNUSED_METHODS.label, true);
        codeSmellStatus.put(UNUSED_PARAMETERS.label, true);

        CodeSmellLimits longMethodLimits = new CodeSmellLimits(LONG_METHOD.label);
        longMethodLimits.updateLimit("Maximum number of lines:", "10");
        codeSmellLimits.put(LONG_METHOD.label, longMethodLimits);

        CodeSmellLimits largeClassLimits = new CodeSmellLimits(LARGE_CLASS.label);
        largeClassLimits.updateLimit("Maximum number of lines:", "200");
        largeClassLimits.updateLimit("Maximum number of fields:", "10");
        largeClassLimits.updateLimit("Maximum number of methods:", "10");
        codeSmellLimits.put(LARGE_CLASS.label, largeClassLimits);

        CodeSmellLimits longParameterListLimits = new CodeSmellLimits(LONG_PARAMETER_LIST.label);
        longParameterListLimits.updateLimit("Maximum number of parameters:", "3");
        codeSmellLimits.put(LONG_PARAMETER_LIST.label, longParameterListLimits);

        CodeSmellLimits primitiveObsessionLimits = new CodeSmellLimits(PRIMITIVE_OBSESSION.label);
        primitiveObsessionLimits.updateLimit("Maximum number of primitives:", "5");
        codeSmellLimits.put(PRIMITIVE_OBSESSION.label, primitiveObsessionLimits);

        CodeSmellLimits switchStatementLimits = new CodeSmellLimits(SWITCH_STATEMENT.label);
        switchStatementLimits.updateLimit("Maximum number of cases:", "10");
        codeSmellLimits.put(SWITCH_STATEMENT.label, switchStatementLimits);
    }

    public static AppSettingsState getInstance() {
        return ApplicationManager.getApplication().getService(AppSettingsState.class);
    }



    @Override
    public @NotNull AppSettingsState getState() {
        return this;
    }

    @Override
    public void loadState(@NotNull AppSettingsState state) {
        XmlSerializerUtil.copyBean(state, this);
    }

}
