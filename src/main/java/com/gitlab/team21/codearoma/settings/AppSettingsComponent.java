package com.gitlab.team21.codearoma.settings;

import com.intellij.ui.components.JBCheckBox;
import com.intellij.ui.components.JBLabel;
import com.intellij.ui.components.JBTextField;
import com.intellij.util.ui.FormBuilder;
import com.intellij.util.ui.JBUI.Borders;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * This class is responsible for creating the UI for the Code Aroma plugin's settings page.
 * View of MVC pattern
 */
public class AppSettingsComponent {

    private final JPanel myMainPanel;
    private final List<JBCheckBox> codeSmellCheckboxes = new ArrayList<>();
    private final List<JBTextField> codeSmellTextFields = new ArrayList<>();
    private JBTextField openAiApiKeyTextField = new JBTextField();

    /**
     * This constructor creates the UI for the settings page.
     */
    public AppSettingsComponent() {
        FormBuilder form = FormBuilder.createFormBuilder();
        AppSettingsState stateSettings = AppSettingsState.getInstance();
        final int standardTopInset = 1;

        // Create a text field for the user's OpenAI API Key (for the Ask LLM feature)
        addSectionHeading(form, "OpenAI API Key");
        JBTextField openAiApiKeyTextField = new JBTextField();
        JBLabel openAiApiKeyLabel = new JBLabel("Enter your OpenAI API key:");
        form.addLabeledComponent(openAiApiKeyLabel, openAiApiKeyTextField, standardTopInset, false);
        this.openAiApiKeyTextField = openAiApiKeyTextField;

        // Create checkboxes for each code smell
        addSectionHeading(form, "Toggle Code Smell Detection");
        for (String codeSmell : stateSettings.codeSmellStatus.keySet()) {
            JBCheckBox newCheckbox = new JBCheckBox(codeSmell);
            form.addComponent(newCheckbox, standardTopInset);
            codeSmellCheckboxes.add(newCheckbox);
        }

        // Add a section break to separate different form sections
        addSectionBreak(form);

        // Create text fields for code smells with adjustable integer limits
        addSectionHeading(form, "Adjust Code Smell Thresholds");

        for (CodeSmellLimits limitsObject : stateSettings.codeSmellLimits.values()) {
            String smellName = limitsObject.getSmellName();
            LinkedHashMap<String, String> limits = limitsObject.getLimits();
            addSubsectionHeading(form, smellName);

            // A code smell can have multiple limits associated with it, so we need to iterate through all of them
            for (Map.Entry<String, String> entry : limits.entrySet()) {
                String limitMessage = entry.getKey();
                String limitValue = entry.getValue();

                JBLabel newTextFieldLabel = new JBLabel(limitMessage);
                JBTextField newTextField = new JBTextField(limitValue);
                String newTextFieldName = smellName + limitMessage;    // used to retrieve text field contents
                newTextField.setName(newTextFieldName);

                form.addLabeledComponent(newTextFieldLabel, newTextField, standardTopInset, false);
                codeSmellTextFields.add(newTextField);
            }
        }

        form.addComponentFillVertically(new JPanel(), 0);
        myMainPanel = form.getPanel();
    }

    private void addSectionHeading(final FormBuilder form, String headingText) {
        final int topPadding = 10;
        final int bottomPadding = 5;
        final int fontSize = 14;

        JBLabel heading = new JBLabel(headingText);
        heading.setFont(heading.getFont().deriveFont(Font.BOLD, fontSize));
        heading.setBorder(Borders.emptyBottom(bottomPadding));
        form.addComponent(heading, topPadding);
    }

    private void addSubsectionHeading(final FormBuilder form, String headingText) {
        final int topPadding = 5;
        final int bottomPadding = 2;
        final int fontSize = 12;

        JBLabel heading = new JBLabel(headingText);
        heading.setFont(heading.getFont().deriveFont(Font.BOLD, fontSize));
        heading.setBorder(Borders.emptyBottom(bottomPadding));
        form.addComponent(heading, topPadding);
    }

    private void addSectionBreak(final FormBuilder form) {
        final int verticalSpace = 5;

        JPanel spacer = new JPanel();
        spacer.setPreferredSize(new Dimension(0, verticalSpace));
        form.addComponent(spacer);
    }

    public JPanel getPanel() {
        return myMainPanel;
    }

    /**
     * @param smellName The name of the code smell whose checkbox status is being retrieved
     * @return The status of the checkbox for the specified code smell
     */
    public boolean getCheckboxStatus(String smellName) {
        JBCheckBox smellBox = codeSmellCheckboxes.stream()
                .filter(smell -> smell.getText().equals(smellName))
                .findFirst().orElseThrow();
        return smellBox.isSelected();
    }

    /**
     * @param smellName The name of the code smell whose checkbox status is being set
     * @param newStatus The new status of the checkbox for the specified code smell
     */
    public void setCheckboxStatus(String smellName, boolean newStatus) {
        JBCheckBox smellBox = codeSmellCheckboxes.stream()
                .filter(smell -> smell.getText().equals(smellName))
                .findFirst().orElseThrow();
        smellBox.setSelected(newStatus);
    }

    /**
     * @param smellName    The name of the code smell whose text field contents are being retrieved
     * @param limitMessage The name of the limit whose text field contents are being retrieved
     * @return The contents of the text field for the specified code smell and limit
     */
    public String getTextFieldText(String smellName, String limitMessage) {
        JBTextField textField = codeSmellTextFields.stream()
                .filter(field -> field.getName().equals(smellName + limitMessage))
                .findFirst().orElseThrow();
        return textField.getText();
    }

    /**
     * @param smellName    The name of the code smell whose text field contents are being set
     * @param limitMessage The name of the limit whose text field contents are being set
     * @param newText      The new contents of the text field for the specified code smell and limit
     */
    public void setTextFieldText(String smellName, String limitMessage, String newText) {
        JBTextField textField = codeSmellTextFields.stream()
                .filter(field -> field.getName().equals(smellName + limitMessage))
                .findFirst().orElseThrow();
        textField.setText(newText);
    }

    /**
     * Gets the contents of the OpenAI API Key text field
     *
     * @return The contents of the OpenAI API Key text field
     */
    public String getOpenAiApiKey() {
        return openAiApiKeyTextField.getText();
    }

    /**
     * Sets the contents of the OpenAI API Key text field
     *
     * @param newOpenApiKey The new contents of the OpenAI API Key text field
     */
    public void setOpenAiApiKey(String newOpenApiKey) {
        openAiApiKeyTextField.setText(newOpenApiKey);
    }
}
