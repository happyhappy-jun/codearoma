package com.gitlab.team21.codearoma.settings;

import com.intellij.openapi.options.Configurable;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * This class is responsible for creating the settings UI and updating the AppSettingsState
 * The controller of MVC pattern
 */
final class AppSettingsConfigurable implements Configurable {

    private AppSettingsComponent componentSettings;

    @Nls(capitalization = Nls.Capitalization.Title)
    @Override
    public String getDisplayName() {
        return "CodeAroma";
    }

    /**
     * create the settings UI
     *
     * @return the settings UI
     */
    @Nullable
    @Override
    public JComponent createComponent() {
        componentSettings = new AppSettingsComponent();
        return componentSettings.getPanel();
    }

    /**
     * @return true if the settings UI has been modified, false otherwise
     */
    @Override
    public boolean isModified() {
        AppSettingsState stateSettings = AppSettingsState.getInstance();
        boolean modified = false;

        // Check if checkboxes are modified
        for (String smell : stateSettings.codeSmellStatus.keySet()) {
            boolean componentStatus = componentSettings.getCheckboxStatus(smell);
            boolean stateStatus = stateSettings.codeSmellStatus.get(smell);
            modified |= componentStatus != stateStatus;
        }

        // Check if text fields are modified
        for (CodeSmellLimits limitsObject : stateSettings.codeSmellLimits.values()) {
            String smellName = limitsObject.getSmellName();
            LinkedHashMap<String, String> stateLimits = limitsObject.getLimits();

            for (Map.Entry<String, String> stateLimit : stateLimits.entrySet()) {
                String limitMessage = stateLimit.getKey();
                String stateText = stateLimit.getValue();
                String componentText = componentSettings.getTextFieldText(smellName, limitMessage);
                modified |= !componentText.equals(stateText);
            }
        }

        // Check if OpenAI API key text field is modified
        modified |= !stateSettings.openAiApiKey.equals(componentSettings.getOpenAiApiKey());

        return modified;
    }

    /**
     * Apply the settings UI to the AppSettingsState
     */
    @Override
    public void apply() {
        AppSettingsState stateSettings = AppSettingsState.getInstance();

        // Update AppSettingsState with values from the checkboxes
        for (String smell : stateSettings.codeSmellStatus.keySet()) {
            boolean componentStatus = componentSettings.getCheckboxStatus(smell);
            stateSettings.codeSmellStatus.put(smell, componentStatus);
        }

        // Update AppSettingsState with values from the text fields
        for (CodeSmellLimits limitsObject : stateSettings.codeSmellLimits.values()) {
            String smellName = limitsObject.getSmellName();
            LinkedHashMap<String, String> stateLimits = limitsObject.getLimits();

            for (Map.Entry<String, String> stateLimit : stateLimits.entrySet()) {
                String limitMessage = stateLimit.getKey();
                String componentText = componentSettings.getTextFieldText(smellName, limitMessage);
                if (isValidInteger(componentText)) {
                    stateLimits.put(limitMessage, componentText);
                }
            }
        }

        // Update AppSettingsState with values from the OpenAI API text field
        stateSettings.openAiApiKey = componentSettings.getOpenAiApiKey();
    }

    /**
     * @param input The string to be checked
     * @return true if the string is a positive integer, false otherwise
     */
    public boolean isValidInteger(String input) {
        String positiveIntegerRegex = "\\d+";
        return input.matches(positiveIntegerRegex);
    }

    /**
     * Reset the settings UI to the values stored in AppSettingsState
     */
    @Override
    public void reset() {
        AppSettingsState stateSettings = AppSettingsState.getInstance();

        // Revert checkboxes to their original values from AppSettingsState
        for (String smell : stateSettings.codeSmellStatus.keySet()) {
            componentSettings.setCheckboxStatus(smell, stateSettings.codeSmellStatus.get(smell));
        }

        // Revert text fields to their original values from AppSettingsState
        for (CodeSmellLimits limitsObject : stateSettings.codeSmellLimits.values()) {
            String smellName = limitsObject.getSmellName();
            LinkedHashMap<String, String> stateLimits = limitsObject.getLimits();
            for (Map.Entry<String, String> stateLimit : stateLimits.entrySet()) {
                componentSettings.setTextFieldText(smellName, stateLimit.getKey(), stateLimit.getValue());
            }
        }

        // Revert OpenAI API key to its original value from AppSettingsState
        componentSettings.setOpenAiApiKey(stateSettings.openAiApiKey);
    }

    /**
     * Dispose of the settings UI
     */
    @Override
    public void disposeUIResources() {
        componentSettings = null;
    }

}
