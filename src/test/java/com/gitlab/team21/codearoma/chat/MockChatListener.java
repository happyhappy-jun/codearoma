package com.gitlab.team21.codearoma.chat;

public class MockChatListener implements ChatListener {
    boolean sending;
    boolean failed;
    boolean responseArrived;
    ChatEvent.Sending event;
    ChatEvent.ResponseArrived response;
    ChatEvent.Failed failedEvent;

    public MockChatListener() {
        this.sending = false;
        this.failed = false;
        this.responseArrived = false;
    }

    @Override
    public void sending(ChatEvent.Sending event) {
        this.sending = true;
        this.event = event;
    }

    @Override
    public void failed(ChatEvent.Failed event) {
        this.failed = true;
        this.failedEvent = event;
    }

    @Override
    public void responseArrived(ChatEvent.ResponseArrived event) {
        this.responseArrived = true;
        this.response = event;
    }

    public boolean getSending() {
        return this.sending;
    }

    public boolean getFailed() {
        return this.failed;
    }

    public boolean getResponseArrived() {
        return this.responseArrived;
    }

    public ChatEvent.Sending getEvent() {
        return this.event;
    }

    public ChatEvent.ResponseArrived getResponseArrivedEvent() {
        return this.response;
    }

    public ChatEvent.Failed getFailedEvent() {
        return this.failedEvent;
    }
}
