package com.gitlab.team21.codearoma.chat;

import com.intellij.testFramework.fixtures.BasePlatformTestCase;

public class ListenerListTest extends BasePlatformTestCase {
    private ListenerList listenerList;
    private ChatListener chatListener;

    public void setUp() throws Exception {
        super.setUp();
        listenerList = new ListenerList();
        chatListener = new ChatListener() {
            @Override
            public void sending(ChatEvent.Sending event) {

            }

            @Override
            public void failed(ChatEvent.Failed event) {

            }

            @Override
            public void responseArrived(ChatEvent.ResponseArrived event) {

            }
        };
    }


    public void testAddListener() {
        listenerList.addListener(chatListener);
        assertNotNull(listenerList);
    }


    public void testRemoveListener() {
        listenerList.removeListener(chatListener);
    }


    public void testFire() {
        ChatListener proxy = listenerList.fire();
        assertNotNull(proxy);
    }
}
