package com.gitlab.team21.codearoma.chat;

import com.gitlab.team21.codearoma.exception.InvalidRequestException;
import com.intellij.testFramework.fixtures.BasePlatformTestCase;

public class EventTest extends BasePlatformTestCase {
    ChatService chatService;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        chatService = new ChatServiceSingleton().getChatServiceInstance();
    }

    public void testResponseArrived() {
        ChatEvent.Sending event = new ChatEvent.Sending(chatService, "test");
        ChatEvent.ResponseArrived response = event.responseArrived("response");
        assertEquals("response", response.getMessage());
    }

    public void testGetMethodName() {
        ChatEvent.Sending event = new ChatEvent.Sending(chatService, "test");
        assertEquals("sending", event.getMethodName());

        ChatEvent.ResponseArrived response = event.responseArrived("response");
        assertEquals("responseArrived", response.getMethodName());
    }

    public void testFailed() {
        ChatEvent.Sending event = new ChatEvent.Sending(chatService, "test");
        ChatEvent.Failed failed = event.failed(new InvalidRequestException("failed"));
        assertEquals("test", failed.getMessage());
        assertEquals("failed", failed.getCause().getMessage());
    }
}
