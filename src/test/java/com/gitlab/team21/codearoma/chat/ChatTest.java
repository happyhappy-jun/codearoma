package com.gitlab.team21.codearoma.chat;

import com.gitlab.team21.codearoma.llm.CallLLMApi;
import com.gitlab.team21.codearoma.llm.model.ConversationManager;
import com.intellij.testFramework.fixtures.BasePlatformTestCase;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.mockito.Mockito;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ChatTest extends BasePlatformTestCase {
    ChatService chatService;


    @Override
    protected void setUp() throws Exception {
        super.setUp();
        chatService = new ChatServiceSingleton().getChatServiceInstance();
    }

    public void testAddChatListener() {
        ChatSubject chatSubject = new ChatSubject();
        ChatListener chatListener = new MockChatListener();
        chatSubject.addChatListener(chatListener);
        assertNotNull(chatSubject);
    }

    public void testSendMessage() {
        ConversationManager conversationManager = new ConversationManager();

        MockChatListener mockChatListener = new MockChatListener();
        chatService.addChatListener(mockChatListener);

        assertFalse(mockChatListener.getSending());
        chatService.sendMessage(conversationManager.toString(), "test");

        assertTrue(mockChatListener.getSending());
    }

    public void testSendMessageWithWait() throws ExecutionException, InterruptedException, IOException {
        CloseableHttpClient closeableHttpClient = mock(CloseableHttpClient.class);
        CloseableHttpResponse closeableHttpResponse = mock(CloseableHttpResponse.class);

        when(closeableHttpClient.execute(Mockito.any(HttpUriRequest.class))).thenReturn(closeableHttpResponse);
        when(closeableHttpResponse.getEntity()).thenReturn(new StringEntity("{\"choices\": []}"));


        CallLLMApi mockedApi = new CallLLMApi(closeableHttpClient);


        ChatService mockedChatService = new ChatService(mockedApi);

        ConversationManager conversationManager = new ConversationManager();

        MockChatListener mockChatListener = new MockChatListener();
        mockedChatService.addChatListener(mockChatListener);

        assertFalse(mockChatListener.getSending());
        Future future = mockedChatService.sendMessage(conversationManager.toString(), "test");
        future.get();

        assertTrue(mockChatListener.getSending());
    }

    public void testSendMessageFailed() throws Exception {
        CallLLMApi mockedApi = mock(CallLLMApi.class);
        when(mockedApi.callApi(Mockito.anyString())).thenThrow(new IOException("failed"));


        ChatService mockedChatService = new ChatService(mockedApi);

        ConversationManager conversationManager = new ConversationManager();

        MockChatListener mockChatListener = new MockChatListener();
        mockedChatService.addChatListener(mockChatListener);

        assertFalse(mockChatListener.getSending());
        Future future = mockedChatService.sendMessage(conversationManager.toString(), "test");
        future.get();

        assertTrue(mockChatListener.getSending());
        assertTrue(mockChatListener.getFailed());
    }


    public void testFailedEvent() throws Exception {
        CallLLMApi mockedApi = mock(CallLLMApi.class);
        when(mockedApi.callApi(Mockito.anyString())).thenThrow(new IOException("failed"));


        ChatService mockedChatService = new ChatService(mockedApi);

        ConversationManager conversationManager = new ConversationManager();

        MockChatListener mockChatListener = new MockChatListener();
        mockedChatService.addChatListener(mockChatListener);

        Future future = mockedChatService.sendMessage(conversationManager.toString(), "test");
        future.get();

        assertTrue(mockChatListener.getFailed());
        assertEquals("failed", mockChatListener.getFailedEvent().getCause().getMessage());
        assertEquals("failed", mockChatListener.getFailedEvent().getMethodName());
    }

    public void testSendingEvent() {
        ChatEvent.Sending event = new ChatEvent.Sending(chatService, "test");
        assertEquals("test", event.getMessage());
        assertEquals("sending", event.getMethodName());
    }

    public void testResponseArrivedEvent() {
        ChatEvent.Sending event = new ChatEvent.Sending(chatService, "test");
        ChatEvent.ResponseArrived res = new ChatEvent.ResponseArrived(event, "test");
        assertEquals("test", res.getMessage());
        assertEquals("responseArrived", res.getMethodName());
    }

}
