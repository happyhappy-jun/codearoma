// Copyright 2000-2023 JetBrains s.r.o. and contributors. Use of this source code is governed by the Apache 2.0 license.
package com.gitlab.team21.codearoma.smell;

import com.gitlab.team21.codearoma.settings.AppSettingsState;
import com.gitlab.team21.codearoma.settings.CodeSmellLimits;
import com.intellij.codeInsight.daemon.impl.HighlightInfo;
import com.intellij.lang.annotation.HighlightSeverity;
import com.intellij.testFramework.TestDataPath;
import com.intellij.testFramework.fixtures.LightJavaCodeInsightFixtureTestCase;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedHashMap;
import java.util.List;

@TestDataPath("$CONTENT_ROOT/testData")
public class ConfigurationTest extends LightJavaCodeInsightFixtureTestCase {

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        myFixture.enableInspections(new DataClassInspection());
        myFixture.enableInspections(new DuplicatedConstructorInspection());
        myFixture.enableInspections(new LargeClassInspection());
        myFixture.enableInspections(new LazyClassInspection());
        myFixture.enableInspections(new LongMethodInspection());
        myFixture.enableInspections(new LongParameterListInspection());
        myFixture.enableInspections(new PrimitiveObsessionInspection());
        myFixture.enableInspections(new RefusedBequestsInspection());
        myFixture.enableInspections(new SwitchStatementInspection());
        myFixture.enableInspections(new UnusedFieldsInspection());
        myFixture.enableInspections(new UnusedMethodsInspection());
        myFixture.enableInspections(new UnusedParametersInspection());
        // optimization: add a fake java.lang.String class to avoid loading all JDK classes for test
        myFixture.addClass("package java.lang; public final class String {}");

        AppSettingsState settings = AppSettingsState.getInstance();
        settings.codeSmellStatus.replaceAll((s, v) -> false);
    }

    /**
     * Defines the path to files used for running tests.
     *
     * @return The path from this module's root directory ($MODULE_WORKING_DIR$) to the directory
     * containing files for these tests.
     */
    @Override
    protected String getTestDataPath() {
        return "src/test/testData/ConfigurationSetting";
    }

    /**
     * Test turn on/off code smell.
     */
    public void testLongMethodToggle() {
        AppSettingsState settings = AppSettingsState.getInstance();
        settings.codeSmellStatus.put("Long Method", true);
        doTest("ConfigurationLongMethod", true);
        settings.codeSmellStatus.put("Long Method", false);
        doTest("ConfigurationLongMethod", false);
    }

    public void testAllCodeSmellToggles() {
        AppSettingsState settings = AppSettingsState.getInstance();
        for (String smell : settings.codeSmellStatus.keySet()) {
            String fileName = "Configuration" + smell.replaceAll(" ", "");

            settings.codeSmellStatus.put(smell, true);
            doTest(fileName, true);
            settings.codeSmellStatus.put(smell, false);
            doTest(fileName, false);
        }
    }

    public void testAllCodeSmellLimits() {
        AppSettingsState settings = AppSettingsState.getInstance();
        for (CodeSmellLimits limitsObject : settings.codeSmellLimits.values()) {
            // Enable the smell detection feature
            String smellName = limitsObject.getSmellName();

            settings.codeSmellStatus.put(smellName, true);
            LinkedHashMap<String, String> limitMap = limitsObject.getLimits();
            String fileName = "Configuration" + smellName.replaceAll(" ", "");

            // For each limit, increase the limit and test that the highlighting disappears
            limitMap.replaceAll((k, v) -> Integer.toString(Integer.parseInt(v) + 1));
            doTest(fileName, false);

            // For each limit, reset the limit back to its original value, test that the highlighting reappears
            limitMap.replaceAll((k, v) -> Integer.toString(Integer.parseInt(v) - 1));
            doTest(fileName, true);

            // Disable the smell detection feature
            settings.codeSmellStatus.put(smellName, false);
        }
    }

    /**
     * Given the name of a test file, runs comparing references inspection quick fix and tests the
     * results against a reference outcome file. File name pattern 'foo.java' and 'foo.after.java'
     * are matching before and after files in the testData directory.
     *
     * @param testName test file name base
     */
    protected void doTest(@NotNull String testName, boolean expected) {
        // Initialize the test based on the testData file
        myFixture.configureByFile(testName + ".java");
        // Initialize the inspection and get a list of highlighted
        List<HighlightInfo> highlightInfos = myFixture.doHighlighting();
        assertEquals(expected, highlightInfos.stream()
                .anyMatch(info -> info.getSeverity() == HighlightSeverity.WARNING));
    }
}
