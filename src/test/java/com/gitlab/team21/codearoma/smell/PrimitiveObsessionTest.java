package com.gitlab.team21.codearoma.smell;

import com.gitlab.team21.codearoma.settings.AppSettingsState;
import com.gitlab.team21.codearoma.settings.CodeSmell;
import com.intellij.codeInsight.daemon.impl.HighlightInfo;
import com.intellij.lang.annotation.HighlightSeverity;
import com.intellij.testFramework.TestDataPath;
import com.intellij.testFramework.fixtures.LightJavaCodeInsightFixtureTestCase;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Tests the PrimitiveObsessionInspection class.
 *
 * @author Jad El Idrissi Dafali
 * @author Nolan D'Souza
 * @see PrimitiveObsessionInspection
 */
@TestDataPath("$CONTENT_ROOT/testData/PrimitiveObsession")
public class PrimitiveObsessionTest extends LightJavaCodeInsightFixtureTestCase {

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        myFixture.enableInspections(new PrimitiveObsessionInspection());
        // optimization: add a fake java.lang.String class to avoid loading all JDK classes for test
        myFixture.addClass("package java.lang; public final class String {}");

        AppSettingsState settings = AppSettingsState.getInstance();
        settings.codeSmellStatus.replaceAll((s, v) -> false);
        settings.codeSmellStatus.put(CodeSmell.PRIMITIVE_OBSESSION.label, true);
    }

    @Override
    protected String getTestDataPath() {
        return "src/test/testData/PrimitiveObsession";
    }

    public void testNoFields() {
        doTest("NoFields", false);
    }

    public void testNoSmell() {
        doTest("NoSmell", false);
    }

    public void testObjects() {
        doTest("NotPrimitive", false);
    }

    public void testPrimitives() {
        doTest("SmellPrimitives", true);
    }

    public void testInts() {
        doTest("Ints", true);
    }

    public void testChars() {
        doTest("Chars", true);
    }

    public void testDoubles() {
        doTest("Doubles", true);
    }

    public void testShorts() {
        doTest("Shorts", true);
    }

    public void testLongs() {
        doTest("Longs", true);
    }

    public void testBytes() {
        doTest("Bytes", true);
    }

    public void testFloats() {
        doTest("Floats", true);
    }

    public void testBooleans() {
        doTest("Booleans", true);
    }

    protected void doTest(@NotNull String testName, boolean expected) {
        // Initialize the test based on the testData file
        myFixture.configureByFile(testName + ".java");
        // Initialize the inspection and get a list of highlighted
        List<HighlightInfo> highlightInfos = myFixture.doHighlighting();
        assertEquals(expected, highlightInfos.stream()
                .anyMatch(info -> info.getSeverity() == HighlightSeverity.WARNING));
    }
}
