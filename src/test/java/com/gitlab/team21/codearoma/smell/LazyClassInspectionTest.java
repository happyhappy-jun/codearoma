package com.gitlab.team21.codearoma.smell;

import com.gitlab.team21.codearoma.settings.AppSettingsState;
import com.gitlab.team21.codearoma.settings.CodeSmell;
import com.intellij.codeInsight.daemon.impl.HighlightInfo;
import com.intellij.lang.annotation.HighlightSeverity;
import com.intellij.testFramework.TestDataPath;
import com.intellij.testFramework.fixtures.LightJavaCodeInsightFixtureTestCase;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Tests the LazyClassInspection class.
 *
 * @author Jikyeong Kim
 * @author Kyumin Park
 * @see LazyClassInspection
 */
@TestDataPath("$CONTENT_ROOT/testData")

public class LazyClassInspectionTest extends LightJavaCodeInsightFixtureTestCase {

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        myFixture.enableInspections(new LazyClassInspection());
        // optimization: add a fake java.lang.String class to avoid loading all JDK classes for test
        myFixture.addClass("package java.lang; public final class String {}");

        AppSettingsState settings = AppSettingsState.getInstance();
        settings.codeSmellStatus.replaceAll((s, v) -> false);
        settings.codeSmellStatus.put(CodeSmell.LAZY_CLASS.label, true);
    }

    @Override
    protected String getTestDataPath() {
        return "src/test/testData/LazyClass";
    }

    public void testUnusedAbstractClass() {
        doTest("UnusedAbstractClass");
    }

    public void testUnusedInterface() {
        doTest("UnusedInterface");
    }

    public void testUnusedClass() {
        doTest("UnusedClass");
    }

    protected void doTest(@NotNull String testName) {
        // Initialize the test based on the testData file
        myFixture.configureByFile(testName + ".java");
        // Initialize the inspection and get a list of highlighted
        List<HighlightInfo> highlightInfos = myFixture.doHighlighting();
        assertTrue(highlightInfos.stream()
                .anyMatch(info -> info.getSeverity() == HighlightSeverity.WARNING));
    }

}
