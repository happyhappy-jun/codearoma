package com.gitlab.team21.codearoma.smell;

import com.gitlab.team21.codearoma.settings.AppSettingsState;
import com.gitlab.team21.codearoma.settings.CodeSmell;
import com.intellij.codeInsight.daemon.impl.HighlightInfo;
import com.intellij.lang.annotation.HighlightSeverity;
import com.intellij.testFramework.TestDataPath;
import com.intellij.testFramework.fixtures.LightJavaCodeInsightFixtureTestCase;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Tests the SwitchStatementInspection class.
 *
 * @author Byungjun Yoon
 * @author Heeju Ku
 * @see SwitchStatementInspection
 */

@TestDataPath("$CONTENT_ROOT/testData/SwitchStatement")
public class DetectingSwitchStatementTest extends LightJavaCodeInsightFixtureTestCase {
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        myFixture.enableInspections(new SwitchStatementInspection());

        AppSettingsState settings = AppSettingsState.getInstance();
        settings.codeSmellStatus.replaceAll((s, v) -> false);
        settings.codeSmellStatus.put(CodeSmell.SWITCH_STATEMENT.label, true);
    }

    @Override
    protected String getTestDataPath() {
        return "src/test/testData/SwitchStatement";
    }

    public void testManyCases() {
        doTest("ManyCases", true);
    }

    public void testFewCases() {
        doTest("FewCases", false);
    }

    protected void doTest(@NotNull String testName, boolean expected) {
        myFixture.configureByFile(testName + ".java");
        List<HighlightInfo> highlightInfos = myFixture.doHighlighting();
        assertEquals(expected, highlightInfos.stream()
                .anyMatch(info -> info.getSeverity() == HighlightSeverity.WARNING));
    }
}
