package com.gitlab.team21.codearoma.smell;

import com.gitlab.team21.codearoma.settings.AppSettingsState;
import com.gitlab.team21.codearoma.settings.CodeSmell;
import com.intellij.codeInsight.daemon.impl.HighlightInfo;
import com.intellij.lang.annotation.HighlightSeverity;
import com.intellij.testFramework.TestDataPath;
import com.intellij.testFramework.fixtures.LightJavaCodeInsightFixtureTestCase;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Tests the DuplicatedConstructorInspection class.
 *
 * @author Kyumin Park
 * @author Heeju Ko
 * @see DuplicatedConstructorInspection
 */
@TestDataPath("/testData/DuplicatedConstructor")
public class DuplicatedConstructorInspectionTest extends LightJavaCodeInsightFixtureTestCase {

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        myFixture.enableInspections(new DuplicatedConstructorInspection());
        // optimization: add a fake java.lang.String class to avoid loading all JDK classes for test
        myFixture.addClass("package java.lang; public final class String {}");

        AppSettingsState settings = AppSettingsState.getInstance();
        settings.codeSmellStatus.replaceAll((s, v) -> false);
        settings.codeSmellStatus.put(CodeSmell.DUPLICATED_CONSTRUCTOR.label, true);
    }

    @Override
    protected String getTestDataPath() {
        return "src/test/testData/DuplicatedConstructor";
    }

    public void testDuplicatedConstructor() {
        doTest("DuplicatedConstructor", true);
    }

    public void testNotDuplicatedConstructor() {
        doTest("NotDuplicatedConstructor", false);
    }

    protected void doTest(@NotNull String testName, boolean expected) {
        // Initialize the test based on the testData file
        myFixture.configureByFiles(testName + ".java");
        // Initialize the inspection and get a list of highlighted
        List<HighlightInfo> highlightInfos = myFixture.doHighlighting();
        assertEquals(expected, highlightInfos.stream()
                .anyMatch(info -> info.getSeverity() == HighlightSeverity.WARNING));
    }
}
