package com.gitlab.team21.codearoma.settings;

import com.intellij.openapi.extensions.ExtensionPoint;
import com.intellij.openapi.extensions.Extensions;
import com.intellij.openapi.options.Configurable;
import com.intellij.openapi.options.ConfigurableEP;
import com.intellij.testFramework.fixtures.BasePlatformTestCase;


public class AppSettingsConfigurableTest extends BasePlatformTestCase {
    AppSettingsConfigurable appSettingsConfigurable;
    AppSettingsState appSettingsState;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        ExtensionPoint<@org.jetbrains.annotations.NotNull ConfigurableEP<Configurable>> extensionPoint = Extensions.getRootArea().getExtensionPoint(Configurable.APPLICATION_CONFIGURABLE);
        ConfigurableEP<Configurable>[] configurableEPs = extensionPoint.getExtensions();

        for (ConfigurableEP<Configurable> ep : configurableEPs) {
            Configurable configurable = ep.createConfigurable();
            if (configurable instanceof AppSettingsConfigurable) {
                appSettingsConfigurable = (AppSettingsConfigurable) configurable;
                break;
            }
        }

        appSettingsState = AppSettingsState.getInstance();
    }

    public void testGetDisplayName() {
        assertEquals("CodeAroma", appSettingsConfigurable.getDisplayName());
    }

    public void testCreateComponent() {
        assertNotNull(appSettingsConfigurable.createComponent());
    }

    public void testIsModified() {
        appSettingsConfigurable.createComponent();
        assertTrue(appSettingsConfigurable.isModified());
    }

    public void testApply() {
        appSettingsConfigurable.createComponent();
        assertTrue(appSettingsConfigurable.isModified());
        appSettingsConfigurable.apply();
        assertFalse(appSettingsConfigurable.isModified());

    }

    public void testIsValidInteger() {
        assertTrue(appSettingsConfigurable.isValidInteger("1"));
    }

    public void testReset() {
        appSettingsConfigurable.createComponent();
        String initial = appSettingsState.getState().openAiApiKey;
        appSettingsConfigurable.reset();
        assertEquals(initial, appSettingsState.getState().openAiApiKey);
    }
}
