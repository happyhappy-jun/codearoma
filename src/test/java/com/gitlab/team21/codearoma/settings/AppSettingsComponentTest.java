package com.gitlab.team21.codearoma.settings;

import com.intellij.testFramework.fixtures.BasePlatformTestCase;

public class AppSettingsComponentTest extends BasePlatformTestCase {
    AppSettingsComponent appSettingsComponent;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        appSettingsComponent = new AppSettingsComponent();
    }

    public void testGetPanel() {
        assertNotNull(appSettingsComponent.getPanel());
    }

    public void testGetCheckboxStatus() {
        assertFalse(appSettingsComponent.getCheckboxStatus("Long Method"));
    }

    public void testSetCheckboxStatus() {
        appSettingsComponent.setCheckboxStatus("Long Method", true);
        assertTrue(appSettingsComponent.getCheckboxStatus("Long Method"));
    }

    public void testGetTextFieldText() {
        AppSettingsState appSettingsState = AppSettingsState.getInstance();
        CodeSmellLimits limits = appSettingsState.codeSmellLimits.get(CodeSmell.LONG_METHOD.label);
        assertEquals("10", limits.getLimits().get("Maximum number of lines:"));
    }

    public void testSetTextFieldText() {
        AppSettingsState appSettingsState = AppSettingsState.getInstance();
        CodeSmellLimits limits = appSettingsState.codeSmellLimits.get(CodeSmell.LONG_METHOD.label);
        assertEquals("10", limits.getLimits().get("Maximum number of lines:"));

        appSettingsComponent.setTextFieldText(CodeSmell.LONG_METHOD.label, "Maximum number of lines:", "20");
        String valueInTextField = appSettingsComponent.getTextFieldText(CodeSmell.LONG_METHOD.label, "Maximum number of lines:");
        assertEquals("20", valueInTextField);
    }

    public void testGetOpenAiApiKey() {
        AppSettingsState appSettingsState = AppSettingsState.getInstance();
        assertEquals("", appSettingsState.openAiApiKey);
    }

    public void testSetOpenAiApiKey() {
        AppSettingsState appSettingsState = AppSettingsState.getInstance();
        assertEquals("", appSettingsState.openAiApiKey);

        appSettingsComponent.setOpenAiApiKey("123");
        String apiKey = appSettingsComponent.getOpenAiApiKey();
        assertEquals("123", apiKey);
    }
}
