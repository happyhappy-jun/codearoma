package com.gitlab.team21.codearoma.settings;

import com.intellij.testFramework.fixtures.BasePlatformTestCase;

public class AppSettingsStateTest extends BasePlatformTestCase {

    public void testGetInstance() {
        AppSettingsState appSettingsState = AppSettingsState.getInstance();
        assertNotNull(appSettingsState);
    }

    public void testLoadState() {
        AppSettingsState appSettingsState = AppSettingsState.getInstance();
        appSettingsState.loadState(new AppSettingsState());
        assertNotNull(appSettingsState);
    }
}
