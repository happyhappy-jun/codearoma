package com.gitlab.team21.codearoma.llm;

import com.gitlab.team21.codearoma.exception.InvalidRequestException;
import com.intellij.testFramework.fixtures.BasePlatformTestCase;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.mockito.Mockito;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CallLLMApiTest extends BasePlatformTestCase {
    CallLLMApi callLLMApi;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        callLLMApi = new CallLLMApi(HttpClients.createDefault());
    }

    public void testCallApi() {
        assertThrows(InvalidRequestException.class, () -> this.callLLMApi.callApi("test"));
    }

    public void testCallApiMock() throws Exception {
        CloseableHttpClient closeableHttpClient = mock(CloseableHttpClient.class);
        CloseableHttpResponse closeableHttpResponse = mock(CloseableHttpResponse.class);

        when(closeableHttpClient.execute(Mockito.any(HttpUriRequest.class))).thenReturn(closeableHttpResponse);
        when(closeableHttpResponse.getEntity()).thenReturn(new StringEntity("{\"choices\": [{\"message\": {\"content\": \"hihi\"}}]}"));


        CallLLMApi mockedApi = new CallLLMApi(closeableHttpClient);
        String context = mockedApi.callApi("hi");
        assertEquals("hihi", context);

    }

    public void testCallApiMockWithNoContent() throws Exception {
        CloseableHttpClient closeableHttpClient = mock(CloseableHttpClient.class);
        CloseableHttpResponse closeableHttpResponse = mock(CloseableHttpResponse.class);

        when(closeableHttpClient.execute(Mockito.any(HttpUriRequest.class))).thenReturn(closeableHttpResponse);
        when(closeableHttpResponse.getEntity()).thenReturn(new StringEntity("{\"choices\": []}"));


        CallLLMApi mockedApi = new CallLLMApi(closeableHttpClient);
        String context = mockedApi.callApi("hi");
        assertEquals("", context);

    }
}
