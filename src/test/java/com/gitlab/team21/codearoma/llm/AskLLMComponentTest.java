package com.gitlab.team21.codearoma.llm;

import com.gitlab.team21.codearoma.chat.ChatService;
import com.gitlab.team21.codearoma.chat.ChatServiceSingleton;
import com.gitlab.team21.codearoma.chat.MockChatListener;
import com.intellij.testFramework.fixtures.BasePlatformTestCase;

public class AskLLMComponentTest extends BasePlatformTestCase {
    AskLLMComponent askLLMComponent;
    CodeInspectionType codeInspectionType;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        codeInspectionType = CodeInspectionType.UnusedParametersInspection;
        askLLMComponent = new AskLLMComponent(codeInspectionType);
    }

    public void testGetName() {
        assertEquals("Ask LLM: unused parameters", askLLMComponent.getName());
    }

    public void testGetFamilyName() {
        assertEquals("Ask LLM: unused parameters", askLLMComponent.getFamilyName());
    }

    public void testApplyFix() {

        MockChatListener mockChatListener = new MockChatListener();
        ChatService chatService = new ChatServiceSingleton().getChatServiceInstance();
        chatService.addChatListener(mockChatListener);
        assertFalse(mockChatListener.getSending());

        AskLLMComponent customServiceLLM = new AskLLMComponent(codeInspectionType, chatService);
        customServiceLLM.applyFix(getProject(), new MockProblemDescriptor());

        assertTrue(mockChatListener.getSending());
    }
}
