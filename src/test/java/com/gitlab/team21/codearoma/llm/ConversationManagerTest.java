package com.gitlab.team21.codearoma.llm;

import com.gitlab.team21.codearoma.llm.model.ConversationManager;
import com.gitlab.team21.codearoma.llm.model.Message;
import com.intellij.testFramework.fixtures.BasePlatformTestCase;

public class ConversationManagerTest extends BasePlatformTestCase {
    ConversationManager testManager;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        testManager = new ConversationManager();
    }


    public void testGetTotalToken() {
        int tokenBefore = testManager.getTotalTokens();
        testManager.addMessage("role", "hi there");
        int tokenAfter = testManager.getTotalTokens();
        assertFalse(tokenBefore == tokenAfter);
    }

    public void testFullMessage() {
        int count = 1;
        String testContent = "I want to graduate ";
        while (!testManager.isFull(testContent + count)) {
            testManager.addMessage("role", testContent + count);
            count++;
        }

        //max token reached
        Message queueFront = testManager.getContext().getMessages().peek();
        testManager.addMessage("role", testContent + count);

        assertNotSame(queueFront, testManager.getContext().getMessages().peek());
    }
}
