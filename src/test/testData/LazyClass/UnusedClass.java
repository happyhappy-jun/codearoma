package LazyClass;

public class UnusedClass {
    public void declaredButNotInstantiated() {
        UnusedClass notUsedClass;
    }
}

// Recommend: to remove not instantiated class "UnusedClass"
