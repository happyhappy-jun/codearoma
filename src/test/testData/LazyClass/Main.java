package LazyClass;

public class Main {
    public static void main() {
        UsedClass aClass = new UsedClass();

        boolean isUnusedClass = UnusedClass.class.isInstance(aClass);
        boolean isUnusedInterface = UnusedInterface.class.isInstance(aClass);
        boolean isUnusedAbstract = UnusedAbstractClass.class.isInstance(aClass);

        System.out.println("" + isUnusedClass + ", " + isUnusedInterface + ", " + isUnusedAbstract + "");
    }
}
