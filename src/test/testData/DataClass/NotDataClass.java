package DataClass;

public class NotDataClass {
    private final String entity1;
    private final int entity2;

    public NotDataClass(String entity, Integer entity2) {
        this.entity1 = entity;
        this.entity2 = entity2;
    }

    public String getEntity1() {
        return entity1;
    }

    public Integer getEntity2() {
        return entity2;
    }

    public void someAction() {
        System.out.println("" + getEntity1() + ", " + getEntity2() + "");
    }
}
