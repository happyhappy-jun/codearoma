package DataClass;

// this class is just for clean GUI testing
// purpose of removing other code smells (e.g. unused method, class, etc)
public class Main {
    public static void main() {
        Record aRecord = new Record("entity1", 1);
        DataClass aClass = new DataClass("entity2", 2);
        NotDataClass notDataClass = new NotDataClass("entity3", 3);
        PublicField publicFieldClass = new PublicField(1, 2);

        System.out.println(aRecord.entity1() + ", " + aRecord.entity2() + ", " + aClass.getEntity1() + ", " + aClass.getEntity2());
        System.out.println(publicFieldClass.sumEntities());
        notDataClass.someAction();
    }
}
