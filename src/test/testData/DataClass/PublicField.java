package DataClass;

public class PublicField {

    private final int entity1;
    public int entity2;

    public PublicField(int entity, int entity2) {
        this.entity1 = entity;
        this.entity2 = entity2;
    }

    public int sumEntities() {
        return entity1 + entity2;
    }

}
