public class Item {
    double total;
    double sale;

    public void calculate(int base, int rate) {
        this.total = base * rate - this.getHalfOfSale();
    }

    public double getHalfOfSale() {
        return this.sale / 2;
    }
}
