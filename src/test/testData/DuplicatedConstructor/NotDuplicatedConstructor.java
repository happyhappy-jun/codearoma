// Example of Duplicated Code in constructor
public class Employee {
    public Employee(String name, String id) {
        this.name = name;
        this.id = id;
    }
}


public class Manager extends Employee {

    public Manager(int grade) {
        this.grade = grade;
    }
}
