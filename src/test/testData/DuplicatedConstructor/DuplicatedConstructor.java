// Example of Duplicated Code in constructor
public class Employee {
    public Employee(String name, String id) {
        this.name = name;
        this.id = id;
    }
}


public class Manager extends Employee {
    public Manager(String name, String id, int grade) {
        this.name = name;
        this.id = id;
        this.grade = grade;
    }
}
