package ConfigurationSetting;

public class ConfigurationUnusedParameters {
    double BASE_RATE = 0.5;
    double total;

    public void calculate(int base, int rate) {
        this.total = base * BASE_RATE;
    }
}

// Recommend: to erase unused parameter "rate"
