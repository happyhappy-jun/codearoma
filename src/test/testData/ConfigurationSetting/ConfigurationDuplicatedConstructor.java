package ConfigurationSetting;

// Example of Duplicated Code in constructor

final public class ConfigurationDuplicatedConstructor extends DuplicatedConstructorParents {
    final int grade;

    public ConfigurationDuplicatedConstructor(String name, String id, int grade) {
        this.name = name;
        this.id = id;
        this.grade = grade;
    }
}

class DuplicatedConstructorParents {
    String name;
    String id;

    public DuplicatedConstructorParents(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public DuplicatedConstructorParents() {
    }
}
