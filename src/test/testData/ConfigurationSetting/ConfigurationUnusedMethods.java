package ConfigurationSetting;

public class ConfigurationUnusedMethods {
    double total;
    double sale;

    public void calculate(int base, int rate) {
        this.total = base * rate - this.sale;
    }

    public double getHalfOfSale() {
        return this.sale / 2;
    }
}

// Recommend: to erase unused methods "getHalfOfSale()"

