package ConfigurationSetting;

public class ConfigurationDataClass {
    private final String entity1;
    private final int entity2;

    public int entity3;

    public ConfigurationDataClass(String entity, Integer entity2) {
        this.entity1 = entity;
        this.entity2 = entity2;
    }

    public String getEntity1() {
        return entity1;
    }

    public Integer getEntity2() {
        return entity2;
    }

    public Integer getEntity3() {
        return entity3;
    }
}
