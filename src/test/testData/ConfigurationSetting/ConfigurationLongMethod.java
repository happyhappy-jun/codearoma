package ConfigurationSetting;

public class ConfigurationLongMethod {
    public double calculateCompoundInterest(int principal, double interestRate, int years) {
        double result = principal;
        double interestRateFactor = 1 + interestRate;
        double compoundFactor = 1;

        for (int i = 0; i < years; i++) {
            compoundFactor *= interestRateFactor;
        }

        return result *= compoundFactor;
    }
}
