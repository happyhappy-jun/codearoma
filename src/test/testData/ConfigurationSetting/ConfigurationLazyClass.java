package ConfigurationSetting;

public class ConfigurationLazyClass {
    public void declaredButNotInstantiated() {
        ConfigurationLazyClass notUsedClass;
    }
}

// Recommend: to remove not instantiated class "UnusedClass"
